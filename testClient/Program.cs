﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace testClient
{
    class Program
    {
        static void Main(string[] args)
        {

            var client = new HttpClient();
            //string baseUrl = "http://195.144.21.184";
            string baseUrl = "http://localhost:8088";
            client.BaseAddress = new Uri(baseUrl);
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "user", "karim" },
                { "pass", "123"},
            };
            var contentData = new FormUrlEncodedContent(values);
            try
            {
                HttpResponseMessage httpresponse = client.PostAsync("PAPI/players/signin?", contentData).Result;
                var result = httpresponse.Content.ReadAsStringAsync().Result.ToString();
                Console.WriteLine(result);

            }
            catch (Exception ex) { }
        }
    }
}
