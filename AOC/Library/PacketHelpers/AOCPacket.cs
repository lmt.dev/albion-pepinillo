﻿using System.Collections.Generic;

namespace AOC.Library.PacketHelpers
{
    public class AOCPacket
    {
        public string Origin { get; set; }
        public byte BaseCode { get; set; }
        public string TranslatedCode { get; set; }
        public Dictionary<byte, object> Parameters { get; set; }
    }
}