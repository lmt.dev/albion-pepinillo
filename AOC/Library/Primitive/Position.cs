﻿namespace AOC.Library.Primitive
{
    public class Position
    {
        public float X { get; set; }
        public float Y { get; set; }
    }
}