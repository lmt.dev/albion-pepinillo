﻿using System;
using AOC.Network.Core;
using AOC.Network.Core.PacketHandler;
using AOC.Network.Data;
using AOC.Network.Packets.Base;

namespace AOC.Library.Util
{
    public static class PhotonParserExtensions
    {
        public static AOCPhotonParser AddEventHandler<TEvent>(this AOCPhotonParser albionParser, EventCodes eventCode, Action<TEvent> action) where TEvent : BaseEvent
        {
            var handler = new EventPacketHandler<TEvent>(eventCode, action);

            albionParser.AddHandler(handler);

            return albionParser;
        }

        public static AOCPhotonParser AddRequestHandler<TRequest>(this AOCPhotonParser albionParser, OperationCodes operationCode, Action<TRequest> action) where TRequest : BaseOpRequest
        {
            var handler = new OpRequestPacketHandler<TRequest>(operationCode, action);

            albionParser.AddHandler(handler);

            return albionParser;
        }

        public static AOCPhotonParser AddResponseHandler<TResponse>(this AOCPhotonParser albionParser, OperationCodes operationCode, Action<TResponse> action) where TResponse : BaseOpResponse
        {
            var handler = new OpResponsePacketHandler<TResponse>(operationCode, action);

            albionParser.AddHandler(handler);

            return albionParser;
        }
    }
}
