﻿using AOC.Library.Configuration.Types;

namespace AOC.Library.Configuration
{
    public class ConfigManager
    {
        public static Sniffing SniffingConfig { get; set; } = Sniffing.GetInstance();
    }
}