﻿using System.IO;
using Newtonsoft.Json;

namespace AOC.Library.Configuration.Types
{
    public class Sniffing
    {
        private static readonly string ConfigPath = "SniffingConfig.json";

        public string SniffingDevice { get; set; }

        /// <summary>
        ///     Loads config if available on disk or creates new if not
        /// </summary>
        /// <returns></returns>
        public static Sniffing GetInstance()
        {
            if (File.Exists(ConfigPath))
            {
                var json = File.ReadAllText(ConfigPath);
                return JsonConvert.DeserializeObject<Sniffing>(json);
            }

            return new Sniffing();
        }

        public void Save()
        {
            File.WriteAllText(ConfigPath, JsonConvert.SerializeObject(this));
        }
    }
}