using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using AOC.Network.Packets.Base;
using AOC.Network.Data;
using System;
using System.Reflection.Metadata.Ecma335;

namespace AOC.Network.Packets.Generated
{

	//Event | BaseCode - 1
	

	//Event | BaseCode - 1
	public class GEventActionOnBuildingFinished : BaseEvent
	{
		// From : "2746856"
		protected System.Int64 Parameter0 {get;set;}
		// From : "637301931145069282"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1756"
		protected System.Int64 Parameter2 {get;set;}
		// From : "22"
		protected System.Int64 Parameter3 {get;set;}
		// From : "5"
		protected System.Int64 Parameter4 {get;set;}
		// From : "55"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventActionOnBuildingFinished(Dictionary<byte,object> data) : base(EventCodes.ActionOnBuildingFinished,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventActionOnBuildingStart : BaseEvent
	{
		// From : "2746856"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1756"
		protected System.Int64 Parameter1 {get;set;}
		// From : "637301931125059680"
		protected System.Int64 Parameter2 {get;set;}
		// From : "637301931145059680"
		protected System.Int64 Parameter3 {get;set;}
		// From : "22"
		protected System.Int64 Parameter4 {get;set;}
		// From : "5"
		protected System.Int64 Parameter5 {get;set;}
		// From : "53"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventActionOnBuildingStart(Dictionary<byte,object> data) : base(EventCodes.ActionOnBuildingStart,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventActiveSpellEffectsUpdate : BaseEvent
	{
		// From : "1034565"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[808,254,364,735,464,741,809,304]"
		protected System.Int64[] Parameter1 {get;set;}
		// From : "[205.48175,187.9568,188.76743,100.0,100.0,100.0,205.48175,188.76743]"
		protected System.Single[] Parameter2 {get;set;}
		// From : "[637301919464760417,637301931941249625,637301931945012811,637301920121149959,637300314100297546,637301900515223923,637301898606993043,637301931951816615]"
		protected System.Int64[] Parameter3 {get;set;}
		// From : "Original \"AQoKAQEBAQo=\" : Converted : \"QVFvS0FRRUJBUW89\""
		protected System.Byte[] Parameter4 {get;set;}
		// From : "Original \"SYIkSZIkSYAkSRI=\" : Converted : \"U1lJa1NaSWtTWUFrU1JJPQ==\""
		protected System.Byte[] Parameter6 {get;set;}
		// From : "Original \"eA==\" : Converted : [101,65,61,61]"
		protected System.Int16[] Parameter8 {get;set;}
		// From : "[9398,0,0,0]"
		protected System.Int64[] Parameter9 {get;set;}
		// From : "10"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventActiveSpellEffectsUpdate(Dictionary<byte,object> data) : base(EventCodes.ActiveSpellEffectsUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToIntArray();
			Parameter4 = SafeExtract(4).ToByteArray();
			Parameter6 = SafeExtract(6).ToByteArray();
			Parameter8 = SafeExtract(8).ToInt16Array();
			Parameter9 = SafeExtract(9).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventAttack : BaseEvent
	{
		// From : "1103399"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46656777"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1099520"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1"
		protected System.Int64 Parameter3 {get;set;}
		// From : "46657027"
		protected System.Int64 Parameter4 {get;set;}
		// From : "46657027"
		protected System.Int64 Parameter5 {get;set;}
		// From : "12"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventAttack(Dictionary<byte,object> data) : base(EventCodes.Attack,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCastCancel : BaseEvent
	{
		// From : "1100099"
		protected System.Int64 Parameter0 {get;set;}
		// From : "true"
		protected System.Boolean Parameter1 {get;set;}
		// From : "1"
		protected System.Int64 Parameter2 {get;set;}
		// From : "15"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastCancel(Dictionary<byte,object> data) : base(EventCodes.CastCancel,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToBoolean();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCastFinished : BaseEvent
	{
		// From : "1100624"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1278"
		protected System.Int64 Parameter2 {get;set;}
		// From : "637301931916839680"
		protected System.Int64 Parameter3 {get;set;}
		// From : "1"
		protected System.Int64 Parameter4 {get;set;}
		// From : "8"
		protected System.Int64 Parameter5 {get;set;}
		// From : "637301931766839680"
		protected System.Int64 Parameter6 {get;set;}
		// From : "17"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastFinished(Dictionary<byte,object> data) : base(EventCodes.CastFinished,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCastHit : BaseEvent
	{
		// From : "1034565"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1034565"
		protected System.Int64 Parameter1 {get;set;}
		// From : "304"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1"
		protected System.Int64 Parameter3 {get;set;}
		// From : "5"
		protected System.Int64 Parameter4 {get;set;}
		// From : "19"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastHit(Dictionary<byte,object> data) : base(EventCodes.CastHit,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCastHits : BaseEvent
	{
		// From : "1103984"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[9398,9398,9398]"
		protected System.Int64[] Parameter1 {get;set;}
		// From : "[735,705,719]"
		protected System.Int64[] Parameter2 {get;set;}
		// From : "\"AQEB\""
		protected System.String Parameter3 {get;set;}
		// From : "\"AAAA\""
		protected System.String Parameter4 {get;set;}
		// From : "20"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastHits(Dictionary<byte,object> data) : base(EventCodes.CastHits,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCastSpell : BaseEvent
	{
		// From : "1034565"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1034565"
		protected System.Int64 Parameter1 {get;set;}
		// From : "[380.6944,-2.2499914]"
		protected System.Single[] Parameter2 {get;set;}
		// From : "304"
		protected System.Int64 Parameter3 {get;set;}
		// From : "46713343"
		protected System.Int64 Parameter4 {get;set;}
		// From : "46713343"
		protected System.Int64 Parameter5 {get;set;}
		// From : "5"
		protected System.Int64 Parameter6 {get;set;}
		// From : "18"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastSpell(Dictionary<byte,object> data) : base(EventCodes.CastSpell,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCastStart : BaseEvent
	{
		// From : "1100624"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46694846"
		protected System.Int64 Parameter1 {get;set;}
		// From : "[342.21408,1.1314353]"
		protected System.Single[] Parameter2 {get;set;}
		// From : "46694846"
		protected System.Int64 Parameter4 {get;set;}
		// From : "1278"
		protected System.Int64 Parameter5 {get;set;}
		// From : "4390"
		protected System.Int64 Parameter6 {get;set;}
		// From : "8"
		protected System.Int64 Parameter8 {get;set;}
		// From : "13"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastStart(Dictionary<byte,object> data) : base(EventCodes.CastStart,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventChannelingEnded : BaseEvent
	{
		// From : "1098066"
		protected System.Int64 Parameter0 {get;set;}
		// From : "3"
		protected System.Int64 Parameter1 {get;set;}
		// From : "18"
		protected System.Int64 Parameter2 {get;set;}
		// From : "21"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventChannelingEnded(Dictionary<byte,object> data) : base(EventCodes.ChannelingEnded,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCharacterEquipmentChanged : BaseEvent
	{
		// From : "1103399"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46670545"
		protected System.Int64 Parameter1 {get;set;}
		// From : "[5277,1479,2583,2606,2629,0,1562,1905,0,0]"
		protected System.Int64[] Parameter2 {get;set;}
		// From : "[-1,1391,1405,1807,1888,1941,-1,-1,-1,-1,-1,-1,-1,-1]"
		protected System.Int64[] Parameter5 {get;set;}
		// From : "true"
		protected System.Boolean Parameter6 {get;set;}
		// From : "80"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCharacterEquipmentChanged(Dictionary<byte,object> data) : base(EventCodes.CharacterEquipmentChanged,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter5 = SafeExtract(5).ToIntArray();
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventCraftItemFinished : BaseEvent
	{
		// From : "2760012"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1757"
		protected System.Int64 Parameter1 {get;set;}
		// From : "[2760516]"
		protected System.Int64[] Parameter2 {get;set;}
		// From : "\"\""
		protected System.String Parameter3 {get;set;}
		// From : "\"\""
		protected System.String Parameter4 {get;set;}
		// From : "62"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCraftItemFinished(Dictionary<byte,object> data) : base(EventCodes.CraftItemFinished,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventDefenseUnitAttackBegin : BaseEvent
	{
		// From : "1103469"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46662985"
		protected System.Int64 Parameter1 {get;set;}
		// From : "0.089999996"
		protected System.Double Parameter2 {get;set;}
		// From : "13209.097"
		protected System.Double Parameter3 {get;set;}
		// From : "0"
		protected System.Int64 Parameter4 {get;set;}
		// From : "0"
		protected System.Int64 Parameter5 {get;set;}
		// From : "2"
		protected System.Int64 Parameter6 {get;set;}
		// From : "688"
		protected System.Int64 Parameter7 {get;set;}
		// From : "281"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDefenseUnitAttackBegin(Dictionary<byte,object> data) : base(EventCodes.DefenseUnitAttackBegin,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventDied : BaseEvent
	{
		// From : "[65.19963,337.28995]"
		protected System.Single[] Parameter0 {get;set;}
		// From : "1104074"
		protected System.Int64 Parameter1 {get;set;}
		// From : "\"Piraxim\""
		protected System.String Parameter2 {get;set;}
		// From : "1104167"
		protected System.Int64 Parameter3 {get;set;}
		// From : "\"te7su\""
		protected System.String Parameter4 {get;set;}
		protected System.String Parameter5 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 {get;set;}
		// From : "153"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDied(Dictionary<byte,object> data) : base(EventCodes.Died,data)
		{
			Parameter0 = SafeExtract(0).ToFloatArray();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			if (SafeExtract(5) == null) { Parameter5 = null; } else { Parameter5 = SafeExtract(5).ToString(); }
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventDuelStarted : BaseEvent
	{
		// From : "1092586"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1102594"
		protected System.Int64 Parameter1 {get;set;}
		// From : "true"
		protected System.Boolean Parameter2 {get;set;}
		// From : "171"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDuelStarted(Dictionary<byte,object> data) : base(EventCodes.DuelStarted,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventEasyAntiCheatMessageToClient : BaseEvent
	{
		// From : "188547"
		protected System.Int64 Parameter0 {get;set;}
		// From : "346"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEasyAntiCheatMessageToClient(Dictionary<byte,object> data) : base(EventCodes.EasyAntiCheatMessageToClient,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventEnergyUpdate : BaseEvent
	{
		// From : "1100624"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46694917"
		protected System.Int64 Parameter1 {get;set;}
		// From : "-27.0"
		protected System.Double Parameter2 {get;set;}
		// From : "176.0"
		protected System.Double Parameter3 {get;set;}
		// From : "7"
		protected System.Int64 Parameter4 {get;set;}
		// From : "1100624"
		protected System.Int64 Parameter5 {get;set;}
		// From : "1278"
		protected System.Int64 Parameter6 {get;set;}
		// From : "7"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnergyUpdate(Dictionary<byte,object> data) : base(EventCodes.EnergyUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventFishingCast : BaseEvent
	{
		// From : "1095092"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1095101"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1887"
		protected System.Int64 Parameter2 {get;set;}
		// From : "4"
		protected System.Int64 Parameter3 {get;set;}
		// From : "637301928655229680"
		protected System.Int64 Parameter4 {get;set;}
		// From : "323"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFishingCast(Dictionary<byte,object> data) : base(EventCodes.FishingCast,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}
	public class GEventFishingFinish : BaseEvent
	{
		// From : "1095092"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1095101"
		protected System.Boolean Parameter1 { get; set; }
		// From : "1887"
		protected System.Int64 Parameter255 { get; set; }
		// From : "4"

		protected System.Int64 Parameter253 { get; set; }
		// From : "637301928655229680"
	
		protected GEventFishingFinish(Dictionary<byte, object> data) : base(EventCodes.FishingFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToBoolean();
			Parameter255 = SafeExtract(2).ToInt();
			Parameter253 = SafeExtract(3).ToInt();
			
		}
	}
	//Event | BaseCode - 1
	public class GEventFishingMiniGame : BaseEvent
	{
		// From : "4957"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[374.8,-4.2]"
		protected System.Single[] Parameter1 {get;set;}
		// From : "9"
		protected System.Int64 Parameter2 {get;set;}
		// From : "6"
		protected System.Int64 Parameter3 {get;set;}
		// From : "\"\""
		protected System.String Parameter4 {get;set;}
		// From : "329"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFishingMiniGame(Dictionary<byte,object> data) : base(EventCodes.FishingMiniGame,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventForcedMovement : BaseEvent
	{
		// From : "1100624"
		protected System.Int64 Parameter0 {get;set;}
		// From : "2"
		protected System.Int64 Parameter1 {get;set;}
		// From : "637301931767551088"
		protected System.Int64 Parameter2 {get;set;}
		// From : "46695350"
		protected System.Int64 Parameter3 {get;set;}
		// From : "30.0"
		protected System.Double Parameter4 {get;set;}
		// From : "[341.17813,11.138637,339.84116,24.069496]"
		protected System.Single[] Parameter5 {get;set;}
		// From : "1278"
		protected System.Int64 Parameter6 {get;set;}
		// From : "1"
		protected System.Int64 Parameter7 {get;set;}
		// From : "132"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventForcedMovement(Dictionary<byte,object> data) : base(EventCodes.ForcedMovement,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToFloatArray();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	

	//Event | BaseCode - 1
	

	//Event | BaseCode - 1
	public class GEventFullExpeditionInfo : BaseEvent
	{
		// From : "1757"
		protected System.Int64 Parameter0 {get;set;}
		// From : "Original \"FAcIM+NJB0aFo7jx+ARp3w==\" : Converted : [70,65,99,73,77,43,78,74,66,48,97,70,111,55,106,120,43,65,82,112,51,119,61,61]"
		protected System.Int16[] Parameter1 {get;set;}
		// From : "20"
		protected System.Int64 Parameter2 {get;set;}
		// From : "0"
		protected System.Int64 Parameter3 {get;set;}
		// From : "637301468640619228"
		protected System.Int64 Parameter4 {get;set;}
		// From : "239"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFullExpeditionInfo(Dictionary<byte,object> data) : base(EventCodes.FullExpeditionInfo,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	

	//Event | BaseCode - 1
	public class GEventHarvestableChangeState : BaseEvent
	{
		// From : "7954"
		protected System.Int64 Parameter0 {get;set;}
		// From : "2"
		protected System.Int64 Parameter1 {get;set;}
		// From : "0"
		protected System.Int64 Parameter2 {get;set;}
		// From : "36"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestableChangeState(Dictionary<byte,object> data) : base(EventCodes.HarvestableChangeState,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventHarvestCancel : BaseEvent
	{
		// From : "1103469"
		protected System.Int64 Parameter0 {get;set;}
		// From : "637301931428749680"
		protected System.Int64 Parameter1 {get;set;}
		// From : "50"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestCancel(Dictionary<byte,object> data) : base(EventCodes.HarvestCancel,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventHarvestFinished : BaseEvent
	{
		// From : "1103469"
		protected System.Int64 Parameter0 {get;set;}
		// From : "637301931432479680"
		protected System.Int64 Parameter1 {get;set;}
		// From : "637301931448232431"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1103981"
		protected System.Int64 Parameter3 {get;set;}
		// From : "3"
		protected System.Int64 Parameter4 {get;set;}
		protected System.Int64 Parameter6 { get; set; }
		// From : "1"
		protected System.Int64 Parameter7 { get; set; }
		protected System.Single[] Parameter8 {get;set;}
		// From : "\"\""
		protected System.String Parameter9 {get;set;}
		// From : "51"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestFinished(Dictionary<byte,object> data) : base(EventCodes.HarvestFinished,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			if (SafeExtract(8).GetType() == typeof(System.String[]))
				Parameter8 = new float[0];
			else
				Parameter8 = SafeExtract(8).ToFloatArray();
			Parameter9 = SafeExtract(9).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventHarvestStart : BaseEvent
	{
		// From : "1103469"
		protected System.Int64 Parameter0 {get;set;}
		// From : "637301931432479680"
		protected System.Int64 Parameter1 {get;set;}
		// From : "637301931432479680"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1103981"
		protected System.Int64 Parameter3 {get;set;}
		// From : "23"
		protected System.Int64 Parameter4 {get;set;}
		// From : "1.5749999"
		protected System.Double Parameter5 {get;set;}
		// From : "1103485"
		protected System.Int64 Parameter6 {get;set;}
		// From : "1861"
		protected System.Int64 Parameter7 {get;set;}
		// From : "49"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestStart(Dictionary<byte,object> data) : base(EventCodes.HarvestStart,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventHealthUpdate : BaseEvent
	{
		// From : "1099520"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46656843"
		protected System.Int64 Parameter1 {get;set;}
		// From : "-25.0"
		protected System.Double Parameter2 {get;set;}
		// From : "1"
		// From : "1543.0"
		protected System.Double Parameter3 { get; set; }
		protected System.Int64 Parameter4 {get;set;}
		// From : "0"
		protected System.Int64 Parameter5 {get;set;}
		// From : "1103469"
		protected System.Int64 Parameter6 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter7 {get;set;}
		// From : "6"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHealthUpdate(Dictionary<byte,object> data) : base(EventCodes.HealthUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventInCombatStateUpdate : BaseEvent
	{
		// From : "-1"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1"
		protected System.Int64 Parameter1 {get;set;}
		// From : "253"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventInCombatStateUpdate(Dictionary<byte,object> data) : base(EventCodes.InCombatStateUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventInventoryPutItem : BaseEvent
	{
		// From : "1101230"
		protected System.Int64 Parameter0 {get;set;}
		// From : "Original \"5gLc6K9WLk+4JYrAQmgLug==\" : Converted : [53,103,76,99,54,75,57,87,76,107,43,52,74,89,114,65,81,109,103,76,117,103,61,61]"
		protected System.Int16[] Parameter2 {get;set;}
		// From : "23"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventInventoryPutItem(Dictionary<byte,object> data) : base(EventCodes.InventoryPutItem,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventItemRerollQualityFinished : BaseEvent
	{
		// From : "2761225"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46640683"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1755"
		protected System.Int64 Parameter2 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter3 {get;set;}
		// From : "58"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventItemRerollQualityFinished(Dictionary<byte,object> data) : base(EventCodes.ItemRerollQualityFinished,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventItemRerollQualityStart : BaseEvent
	{
		// From : "2761225"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46640558"
		protected System.Int64 Parameter1 {get;set;}
		// From : "46640683"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1755"
		protected System.Int64 Parameter3 {get;set;}
		// From : "56"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventItemRerollQualityStart(Dictionary<byte,object> data) : base(EventCodes.ItemRerollQualityStart,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventJoinFinished : BaseEvent
	{
		// From : "2"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventJoinFinished(Dictionary<byte,object> data) : base(EventCodes.JoinFinished,data)
		{
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventLeave : BaseEvent
	{
		// From : "1093965"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventLeave(Dictionary<byte,object> data) : base(EventCodes.Leave,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventMinimapSmartClusterZergs : BaseEvent
	{
		// From : "[322.67053,-3.8126454,-370.89264,6.156483]"
		protected System.Single[] Parameter0 {get;set;}
		// From : "Original \"JA4=\" : Converted : \"SkE0PQ==\""
		protected System.Int16[] Parameter1 {get;set;}
		// From : "Original \"/wI=\" : Converted : \"L3dJPQ==\""
		protected System.Int16[] Parameter2 {get;set;}
		// From : "304"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMinimapSmartClusterZergs(Dictionary<byte,object> data) : base(EventCodes.MinimapSmartClusterZergs,data)
		{
			Parameter0 = SafeExtract(0).ToFloatArray();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}
	public class GEventMobChangeState : BaseEvent
	{
		// From : "942872"
		protected System.Int64 Parameter0 { get; set; }
		// From : "0"
		protected System.Int64 Parameter1 { get; set; }
		// From : "36"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMobChangeState(Dictionary<byte, object> data) : base(EventCodes.MobChangeState, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventMountCancel : BaseEvent
	{
		// From : "1103399"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46672732"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1905"
		protected System.Int64 Parameter2 {get;set;}
		// From : "true"
		protected System.Boolean Parameter3 {get;set;}
		// From : "196"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMountCancel(Dictionary<byte,object> data) : base(EventCodes.MountCancel,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventMountCooldownUpdate : BaseEvent
	{
		// From : "1092130"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46557488"
		protected System.Int64 Parameter1 {get;set;}
		// From : "-42.0"
		protected System.Double Parameter2 {get;set;}
		// From : "145.0"
		protected System.Double Parameter3 {get;set;}
		// From : "1"
		protected System.Int64 Parameter4 {get;set;}
		// From : "0"
		protected System.Int64 Parameter5 {get;set;}
		// From : "1044082"
		protected System.Int64 Parameter6 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter7 {get;set;}
		// From : "288"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMountCooldownUpdate(Dictionary<byte,object> data) : base(EventCodes.MountCooldownUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventMounted : BaseEvent
	{
		// From : "1687"
		protected System.Int64 Parameter0 {get;set;}
		// From : "3"
		protected System.Int64 Parameter1 {get;set;}
		// From : "[\"@_Owner\",\"@_Guild\",\"@_Everyone\",\"@P_cdc8740d-04dd-4158-9f2c-6d2d185aa24f\",\"@P_a0503948-f21f-4f97-9db2-b9ebd2975198\",\"@P_e7579c9e-6a7f-4524-96b4-b1c7634f5290\",\"@P_66aa8cf0-631d-48b2-ba9e-3b5edbb61103\",\"@P_a71fd072-ab24-4d9d-a5a3-9353915014a2\",\"@G_96f75970-23d0-4a2b-a092-5af91384fee6\",\"@G_b476cf2b-129e-4d33-9f94-8fce0b5a6e52\",\"@G_17a39eeb-0d1e-4706-9370-c8f6f44cb046\",\"@G_13d802bc-a485-4e25-b58c-ac1cc0412089\",\"@P_52c569de-f0a3-4abf-b90e-d45c739dda66\"]"
		protected System.String[] Parameter4 {get;set;}
		// From : "[\"owner\",\"friend\",\"user\",\"friend\",\"friend\",\"friend\",\"friend\",\"friend\",\"friend\",\"friend\",\"friend\",\"friend\",\"friend\"]"
		protected System.String[] Parameter5 {get;set;}
		// From : "[\"\",\"Crimson Imperium Reborn\",\"\",\"GuiMito\",\"Maciaciello\",\"Om1cronPlayer\",\"Juanita2\",\"Espaditaman\",\"Deorum Mortis\",\"Crimson Imperium Newborn\",\"We Craft\",\"Tidal\",\"Inseln5\"]"
		protected System.String[] Parameter6 {get;set;}
		// From : "194"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMounted(Dictionary<byte,object> data) : base(EventCodes.Mounted,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter5 = SafeExtract(5).ToStringArray();
			Parameter6 = SafeExtract(6).ToStringArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventMountHealthUpdate : BaseEvent
	{
		// From : "1103792"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1895"
		protected System.Int64 Parameter1 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter2 {get;set;}
		// From : "[328.1363,3.1911395]"
		protected System.Single[] Parameter3 {get;set;}
		// From : "282.41577"
		protected System.Double Parameter4 {get;set;}
		// From : "Original \"bQhIetJ6S0K07vIqPiIPhw==\" : Converted : [98,81,104,73,101,116,74,54,83,48,75,48,55,118,73,113,80,105,73,80,104,119,61,61]"
		protected System.Int16[] Parameter5 {get;set;}
		// From : "0"
		protected System.Int64 Parameter6 {get;set;}
		// From : "3"
		protected System.Int64 Parameter7 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter11 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter12 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter13 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter14 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter15 {get;set;}
		// From : "1.0"
		protected System.Double Parameter17 {get;set;}
		// From : "287"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMountHealthUpdate(Dictionary<byte,object> data) : base(EventCodes.MountHealthUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt16Array();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToInt();
			Parameter17 = SafeExtract(17).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventMountStart : BaseEvent
	{
		// From : "1104924"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46711084"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1927"
		protected System.Int64 Parameter2 {get;set;}
		// From : "676.0"
		protected System.Double Parameter4 {get;set;}
		// From : "6.76"
		protected System.Double Parameter5 {get;set;}
		// From : "637301931929207427"
		protected System.Int64 Parameter6 {get;set;}
		// From : "true"
		protected System.Boolean Parameter7 {get;set;}
		// From : "true"
		protected System.Boolean Parameter9 {get;set;}
		// From : "-1.0"
		protected System.Double Parameter10 {get;set;}
		// From : "true"
		protected System.Boolean Parameter11 {get;set;}
		// From : "195"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMountStart(Dictionary<byte,object> data) : base(EventCodes.MountStart,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToBoolean();
			Parameter9 = SafeExtract(9).ToBoolean();
			Parameter10 = SafeExtract(10).ToDouble();
			Parameter11 = SafeExtract(11).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 2
	public class GEventMove : BaseEvent
	{
		// From : "1092986"
		protected System.Int64 Parameter0 {get;set;}
		// From : "Original \"A2A+uUC6JtgI0gN0QxFKFENHzMw4Qch5eUOKXBNDAAA=\" : Converted : \"QTJBK3VVQzZKdGdJMGdOMFF4RktGRU5Iek13NFFjaDVlVU9LWEJOREFBQT0=\""
		protected System.Byte[] Parameter1 {get;set;}
		// From : "3"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMove(Dictionary<byte,object> data) : base(EventCodes.Move,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToByteArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewBuilding : BaseEvent
	{
		// From : "1755"
		protected System.Int64 Parameter0 {get;set;}
		// From : "Original \"zERHe62KvU6zMpI0Afev1g==\" : Converted : [122,69,82,72,101,54,50,75,118,85,54,122,77,112,73,48,65,102,101,118,49,103,61,61]"
		protected System.Int16[] Parameter1 {get;set;}
		// From : "260"
		protected System.Int64 Parameter2 {get;set;}
		// From : "\"FOREST_GREEN_REPAIRSHOP\""
		protected System.String Parameter3 {get;set;}
		// From : "[-55.0,-25.0]"
		protected System.Single[] Parameter4 {get;set;}
		// From : "90.0"
		protected System.Double Parameter5 {get;set;}
		// From : "\"System\""
		protected System.String Parameter8 {get;set;}
		// From : "\"System\""
		protected System.String Parameter9 {get;set;}
		// From : "true"
		protected System.Boolean Parameter13 {get;set;}
		// From : "17496000000"
		protected System.Int64 Parameter16 {get;set;}
		// From : "637213418451607756"
		protected System.Int64 Parameter17 {get;set;}
		// From : "7950000000"
		protected System.Int64 Parameter18 {get;set;}
		// From : "637301468640559013"
		protected System.Int64 Parameter19 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter20 {get;set;}
		// From : "637301468640569053"
		protected System.Int64 Parameter21 {get;set;}
		// From : "7949930000"
		protected System.Int64 Parameter22 {get;set;}
		// From : "637213418451587682"
		protected System.Int64 Parameter23 {get;set;}
		// From : "true"
		protected System.Boolean Parameter27 {get;set;}
		// From : "true"
		protected System.Boolean Parameter28 {get;set;}
		// From : "true"
		protected System.Boolean Parameter29 {get;set;}
		// From : "0"
		protected System.Int64 Parameter30 {get;set;}
		// From : "35"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewBuilding(Dictionary<byte,object> data) : base(EventCodes.NewBuilding,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToFloatArray();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter8 = SafeExtract(8).ToString();
			Parameter9 = SafeExtract(9).ToString();
			Parameter13 = SafeExtract(13).ToBoolean();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter17 = SafeExtract(17).ToInt();
			Parameter18 = SafeExtract(18).ToInt();
			Parameter19 = SafeExtract(19).ToInt();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter21 = SafeExtract(21).ToInt();
			Parameter22 = SafeExtract(22).ToInt();
			Parameter23 = SafeExtract(23).ToInt();
			Parameter27 = SafeExtract(27).ToBoolean();
			Parameter28 = SafeExtract(28).ToBoolean();
			Parameter29 = SafeExtract(29).ToBoolean();
			Parameter30 = SafeExtract(30).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewCharacter : BaseEvent
	{
		// From : "1104924"
		protected System.Int64 Parameter0 {get;set;}
		// From : "\"Dxkills\""
		protected System.String Parameter1 {get;set;}
		// From : "26"
		protected System.Int64 Parameter2 {get;set;}
		// From : "23"
		protected System.Int64 Parameter3 {get;set;}
		// From : "Original \"AwACAwQ=\" : Converted : \"QXdBQ0F3UT0=\""
		protected System.Byte[] Parameter5 {get;set;}
		// From : "Original \"AAIIAAU=\" : Converted : \"QUFJSUFBVT0=\""
		protected System.Byte[] Parameter6 {get;set;}
		// From : "Original \"6HIAeeCQp0e/KbeYz/1pcQ==\" : Converted : [54,72,73,65,101,101,67,81,112,48,101,47,75,98,101,89,122,47,49,112,99,81,61,61]"
		protected System.Int16[] Parameter7 {get;set;}
		// From : "\"Heroes of Judgment\""
		protected System.String Parameter8 {get;set;}
		// From : "Original \"ScbXJp8zcUiZXey852+AhA==\" : Converted : [83,99,98,88,74,112,56,122,99,85,105,90,88,101,121,56,53,50,43,65,104,65,61,61]"
		protected System.Int16[] Parameter9 {get;set;}
		// From : "\"\""
		protected System.String Parameter10 {get;set;}
		// From : "\"\""
		protected System.String Parameter11 {get;set;}
		// From : "[370.0,1.9669533E-06]"
		protected System.Single[] Parameter12 {get;set;}
		// From : "[370.0,1.9669533E-06]"
		protected System.Single[] Parameter13 {get;set;}
		// From : "46711082"
		protected System.Int64 Parameter14 {get;set;}
		// From : "271.27304"
		protected System.Double Parameter15 {get;set;}
		// From : "9.625"
		protected System.Double Parameter16 {get;set;}
		// From : "2381.0"
		protected System.Double Parameter18 {get;set;}
		// From : "2381.0"
		protected System.Double Parameter19 {get;set;}
		// From : "42.858112"
		protected System.Double Parameter20 {get;set;}
		// From : "46711082"
		protected System.Int64 Parameter21 {get;set;}
		// From : "372.0"
		protected System.Double Parameter22 {get;set;}
		// From : "372.0"
		protected System.Double Parameter23 {get;set;}
		// From : "5.213741"
		protected System.Double Parameter24 {get;set;}
		// From : "46711082"
		protected System.Int64 Parameter25 {get;set;}
		// From : "676.0"
		protected System.Double Parameter26 {get;set;}
		// From : "676.0"
		protected System.Double Parameter27 {get;set;}
		// From : "6.76"
		protected System.Double Parameter28 {get;set;}
		// From : "46711082"
		protected System.Int64 Parameter29 {get;set;}
		// From : "26912.832"
		protected System.Double Parameter30 {get;set;}
		// From : "637253516208323686"
		protected System.Int64 Parameter31 {get;set;}
		// From : "0"
		protected System.Int64 Parameter32 {get;set;}
		// From : "[4806,1411,3205,3043,3065,1791,1624,1927,0,0]"
		protected System.Int64[] Parameter33 { get; set; }
		// From : "[1472,1474,1483,1784,1875,1923,-1,-1,-1,-1,-1,-1,2288,2141]"
		protected System.Int64[] Parameter35 {get;set;}
		// From : "4"
		protected System.Int64 Parameter36 {get;set;}
		// From : "5"
		protected System.Int64 Parameter37 {get;set;}
		// From : "2"
		protected System.Int64 Parameter38 {get;set;}
		// From : "9"
		protected System.Int64 Parameter39 {get;set;}
		// From : "15775037"
		protected System.Int64 Parameter40 {get;set;}
		// From : "0.90000004"
		protected System.Double Parameter41 {get;set;}
		// From : "0.15"
		protected System.Double Parameter42 {get;set;}
		// From : "\"ARCH\""
		protected System.String Parameter43 {get;set;}
		// From : "Original \"wqgahVsvDk+o44R0bsmIUw==\" : Converted : [119,113,103,97,104,86,115,118,68,107,43,111,52,52,82,48,98,115,109,73,85,119,61,61]"
		protected System.Int16[] Parameter44 {get;set;}
		// From : "2"
		protected System.Int64 Parameter45 {get;set;}
		// From : "[-1,-1,-1,-1,-1,-1,-1]"
		protected System.Int64[] Parameter48 {get;set;}
		// From : "25"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewCharacter(Dictionary<byte,object> data) : base(EventCodes.NewCharacter,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter5 = SafeExtract(5).ToByteArray();
			Parameter6 = SafeExtract(6).ToByteArray();
			Parameter7 = SafeExtract(7).ToInt16Array();
			if (SafeExtract(8) == null)
				Parameter8 = "";
			else
				Parameter8 = SafeExtract(8).ToString();
			Parameter9 = SafeExtract(9).ToInt16Array();
			if (SafeExtract(10) == null)
				Parameter10 = "";
			else
				Parameter10 = SafeExtract(10).ToString();
			if(SafeExtract(11)== null)
				Parameter11 = "";
			else
				Parameter11 = SafeExtract(11).ToString();
			Parameter12 = SafeExtract(12).ToFloatArray();
			Parameter13 = SafeExtract(13).ToFloatArray();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToDouble();
			Parameter16 = SafeExtract(16).ToDouble();
			Parameter18 = SafeExtract(18).ToDouble();
			Parameter19 = SafeExtract(19).ToDouble();
			Parameter20 = SafeExtract(20).ToDouble();
			Parameter21 = SafeExtract(21).ToInt();
			Parameter22 = SafeExtract(22).ToDouble();
			Parameter23 = SafeExtract(23).ToDouble();
			Parameter24 = SafeExtract(24).ToDouble();
			Parameter25 = SafeExtract(25).ToInt();
			Parameter26 = SafeExtract(26).ToDouble();
			Parameter27 = SafeExtract(27).ToDouble();
			Parameter28 = SafeExtract(28).ToDouble();
			Parameter29 = SafeExtract(29).ToInt();
			Parameter30 = SafeExtract(30).ToDouble();
			Parameter31 = SafeExtract(31).ToInt();
			Parameter32 = SafeExtract(32).ToInt();
			if(SafeExtract(33) == null)
				Parameter33 = new Int64[0];
			else
				Parameter33 = SafeExtract(33).ToIntArray();

			if (SafeExtract(35) == null)
				Parameter35 = new Int64[0];
			else
				Parameter35 = SafeExtract(35).ToIntArray();

			Parameter36 = SafeExtract(36).ToInt();
			Parameter37 = SafeExtract(37).ToInt();
			Parameter38 = SafeExtract(38).ToInt();
			Parameter39 = SafeExtract(39).ToInt();
			Parameter40 = SafeExtract(40).ToInt();
			Parameter41 = SafeExtract(41).ToDouble();
			Parameter42 = SafeExtract(42).ToDouble();
			if (SafeExtract(43) == null)
				Parameter43 = "";
			else
				Parameter43 = SafeExtract(43).ToString();
			Parameter44 = SafeExtract(44).ToInt16Array();
			Parameter45 = SafeExtract(45).ToInt();
			if (SafeExtract(48) == null)
				Parameter48 = new Int64[0];
			else
				Parameter48 = SafeExtract(48).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewEquipmentItem : BaseEvent
	{
		// From : "1103815"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1895"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1"
		protected System.Int64 Parameter2 {get;set;}
		// From : "\"Sillyfrog\""
		protected System.String Parameter3 {get;set;}
		// From : "3"
		protected System.Int64 Parameter4 {get;set;}
		// From : "106200000"
		protected System.Int64 Parameter5 {get;set;}
		// From : "\"\""
		protected System.String Parameter6 {get;set;}
		// From : "Original \"iw==\" : Converted : [105,119,61,61]"
		protected System.Int16[] Parameter7 {get;set;}
		// From : "true"
		protected System.Boolean Parameter8 {get;set;}
		// From : "26"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewEquipmentItem(Dictionary<byte,object> data) : base(EventCodes.NewEquipmentItem,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToString();
			Parameter7 = SafeExtract(7).ToInt16Array();
			Parameter8 = SafeExtract(8).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewExit : BaseEvent
	{
		// From : "15"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[-53.0,0.0]"
		protected System.Single[] Parameter1 {get;set;}
		// From : "90.0"
		protected System.Double Parameter2 {get;set;}
		// From : "Original \"8QeoNME66Eah2RhqVrBEPg==\" : Converted : [56,81,101,111,78,77,69,54,54,69,97,104,50,82,104,113,86,114,66,69,80,103,61,61]"
		protected System.Int16[] Parameter3 {get;set;}
		// From : "199"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewExit(Dictionary<byte,object> data) : base(EventCodes.NewExit,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewExpeditionAgent : BaseEvent
	{
		// From : "1103812"
		protected System.Int64 Parameter0 {get;set;}
		// From : "637301838524798214"
		protected System.Int64 Parameter1 {get;set;}
		// From : "300000000"
		protected System.Int64 Parameter2 {get;set;}
		// From : "289"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewExpeditionAgent(Dictionary<byte,object> data) : base(EventCodes.NewExpeditionAgent,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewFishingZoneObject : BaseEvent
	{
		// From : "1098215"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[245.26498,105.439095]"
		protected System.Single[] Parameter1 {get;set;}
		// From : "274.37933"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1095092"
		protected System.Int64 Parameter3 {get;set;}
		// From : "1"
		protected System.String Parameter4 {get;set;}
		// From : "328"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewFishingZoneObject(Dictionary<byte,object> data) : base(EventCodes.NewFishingZoneObject,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewFloatObject : BaseEvent
	{
		// From : "1095092"
		protected System.Int64 Parameter0 {get;set;}
		// From : "9"
		protected System.Int64 Parameter1 {get;set;}
		// From : "327"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewFloatObject(Dictionary<byte,object> data) : base(EventCodes.NewFloatObject,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewHarvestableObject : BaseEvent
	{
		// From : "1103981"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1099520"
		protected System.Int64 Parameter1 {get;set;}
		// From : "637301931386817267"
		protected System.Int64 Parameter2 {get;set;}
		// From : "Original \"xi48rIzG2UObF3UbhhAAzg==\" : Converted : [120,105,52,56,114,73,122,71,50,85,79,98,70,51,85,98,104,104,65,65,122,103,61,61]"
		protected System.Int16[] Parameter3 {get;set;}
		// From : "39127"
		protected System.Int64 Parameter4 {get;set;}
		// From : "23"
		protected System.Int64 Parameter5 {get;set;}
		// From : "30"
		protected System.Int64 Parameter6 {get;set;}
		// From : "3"
		protected System.Int64 Parameter7 {get;set;}
		// From : "[317.7804,-25.932026]"
		protected System.Single[] Parameter8 {get;set;}
		// From : "306.25967"
		protected System.Double Parameter9 {get;set;}
		// From : "3"
		protected System.Int64 Parameter10 {get;set;}
		// From : "0"
		protected System.Int64 Parameter11 {get;set;}
		// From : "33"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewHarvestableObject(Dictionary<byte,object> data) : base(EventCodes.NewHarvestableObject,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToFloatArray();
			Parameter9 = SafeExtract(9).ToDouble();
			Parameter10 = SafeExtract(10).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewHellgate : BaseEvent
	{
		// From : "Original \"AAECAwQFDA0=\" : Converted : \"QUFFQ0F3UUZEQTA9\""
		protected System.Byte[] Parameter0 {get;set;}
		// From : "[46664116,46664116,46664116,46664116,46664116,46664116,46664116,46664116]"
		protected System.Int64[] Parameter1 {get;set;}
		// From : "[46649116,46649116,46649116,46649116,46649116,46649116,46649116,46649116]"
		protected System.Int64[] Parameter2 {get;set;}
		// From : "227"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewHellgate(Dictionary<byte,object> data) : base(EventCodes.NewHellgate,data)
		{
			Parameter0 = SafeExtract(0).ToByteArray();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewLoot : BaseEvent
	{
		// From : "1068506"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1063660"
		protected System.Int64 Parameter2 {get;set;}
		// From : "\"Nomario\""
		protected System.String Parameter3 {get;set;}
		// From : "[367.1786,2.0713649]"
		protected System.Single[] Parameter4 {get;set;}
		// From : "86.05042"
		protected System.Double Parameter5 {get;set;}
		// From : "true"
		protected System.Boolean Parameter6 {get;set;}
		// From : "1"
		protected System.Int64 Parameter7 {get;set;}
		// From : "Original \"ltEHtI4slkGQeQiwwNNb8A==\" : Converted : [108,116,69,72,116,73,52,115,108,107,71,81,101,81,105,119,119,78,78,98,56,65,61,61]"
		protected System.Int16[] Parameter8 {get;set;}
		// From : "637301915653287622"
		protected System.Int64 Parameter9 {get;set;}
		// From : "[]"
		protected System.Single[] Parameter15 {get;set;}
		// From : "Original \"eW+xXIbrF0iI5A+Wa/qNJQ==\" : Converted : [101,87,43,120,88,73,98,114,70,48,105,73,53,65,43,87,97,47,113,78,74,81,61,61]"
		protected System.Int16[] Parameter16 {get;set;}
		// From : "Original \"CDqifEgc30mRKEwNQjmHmw==\" : Converted : [67,68,113,105,102,69,103,99,51,48,109,82,75,69,119,78,81,106,109,72,109,119,61,61]"
		protected System.Int16[] Parameter17 {get;set;}
		// From : "2"
		protected System.Int64 Parameter19 {get;set;}
		// From : "3"
		protected System.Int64 Parameter20 {get;set;}
		// From : "88"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewLoot(Dictionary<byte,object> data) : base(EventCodes.NewLoot,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToFloatArray();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt16Array();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter15 = SafeExtract(15).ToFloatArray();
			Parameter16 = SafeExtract(16).ToInt16Array();
			Parameter17 = SafeExtract(17).ToInt16Array();
			Parameter19 = SafeExtract(19).ToInt();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewMob : BaseEvent
	{
		// From : "1084769"
		protected System.Int64 Parameter0 {get;set;}
		// From : "31"
		protected System.Int64 Parameter1 {get;set;}
		// From : "255"
		protected System.Int64 Parameter2 {get;set;}
		// From : "\"\""
		protected System.String Parameter6 {get;set;}
		// From : "[315.23206,40.373615]"
		protected System.Single[] Parameter7 {get;set;}
		// From : "[315.28134,40.37944]"
		protected System.Single[] Parameter8 {get;set;}
		// From : "46690061"
		protected System.Int64 Parameter9 {get;set;}
		// From : "83.26096"
		protected System.Double Parameter10 {get;set;}
		// From : "4.5"
		protected System.Double Parameter11 {get;set;}
		// From : "1323.0"
		protected System.Double Parameter13 {get;set;}
		// From : "1323.0"
		protected System.Double Parameter14 {get;set;}
		// From : "46553610"
		protected System.Int64 Parameter16 {get;set;}
		// From : "126.0"
		protected System.Double Parameter17 {get;set;}
		// From : "126.0"
		protected System.Double Parameter18 {get;set;}
		// From : "4.0"
		protected System.Double Parameter19 {get;set;}
		// From : "46689860"
		protected System.Int64 Parameter20 {get;set;}
		// From : "0"
		protected System.Int64 Parameter29 {get;set;}
		// From : "115"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewMob(Dictionary<byte,object> data) : base(EventCodes.NewMob,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter6 = SafeExtract(6).ToString();
			Parameter7 = SafeExtract(7).ToFloatArray();
			Parameter8 = SafeExtract(8).ToFloatArray();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter10 = SafeExtract(10).ToDouble();
			Parameter11 = SafeExtract(11).ToDouble();
			Parameter13 = SafeExtract(13).ToDouble();
			Parameter14 = SafeExtract(14).ToDouble();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter17 = SafeExtract(17).ToDouble();
			Parameter18 = SafeExtract(18).ToDouble();
			Parameter19 = SafeExtract(19).ToDouble();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter29 = SafeExtract(29).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewMonolithObject : BaseEvent
	{
		// From : "19"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[10.0,16.0]"
		protected System.Single[] Parameter1 {get;set;}
		// From : "Original \"hY19asSLa0qhzsm8iMr/5g==\" : Converted : [104,89,49,57,97,115,83,76,97,48,113,104,122,115,109,56,105,77,114,47,53,103,61,61]"
		protected System.Int16[] Parameter3 {get;set;}
		// From : "\"Elevate\""
		protected System.String Parameter4 {get;set;}
		// From : "Original \"Ndx18+ew7k2lXelFlUq1lw==\" : Converted : [78,100,120,49,56,43,101,119,55,107,50,108,88,101,108,70,108,85,113,49,108,119,61,61]"
		protected System.Int16[] Parameter5 {get;set;}
		// From : "Original \"mDhrtRso+U2QNtYy5Z4tww==\" : Converted : [109,68,104,114,116,82,115,111,43,85,50,81,78,116,89,121,53,90,52,116,119,119,61,61]"
		protected System.Int16[] Parameter6 {get;set;}
		// From : "0"
		protected System.Int64 Parameter7 {get;set;}
		// From : "69350000"
		protected System.Int64 Parameter9 {get;set;}
		// From : "0"
		protected System.Int64 Parameter11 {get;set;}
		// From : "5"
		protected System.Int64 Parameter13 {get;set;}
		// From : "30"
		protected System.Int64 Parameter14 {get;set;}
		// From : "10"
		protected System.Int64 Parameter15 {get;set;}
		// From : "2"
		protected System.Int64 Parameter16 {get;set;}
		// From : "16777215"
		protected System.Int64 Parameter17 {get;set;}
		// From : "0.85"
		protected System.Double Parameter18 {get;set;}
		// From : "-0.1"
		protected System.Double Parameter19 {get;set;}
		// From : "0"
		protected System.Int64 Parameter20 {get;set;}
		// From : "99"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewMonolithObject(Dictionary<byte,object> data) : base(EventCodes.NewMonolithObject,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter4 = SafeExtract(4).ToString();
			Parameter5 = SafeExtract(5).ToInt16Array();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToInt();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter17 = SafeExtract(17).ToInt();
			Parameter18 = SafeExtract(18).ToDouble();
			Parameter19 = SafeExtract(19).ToDouble();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewOrbObject : BaseEvent
	{
		// From : "13"
		protected System.Int64 Parameter0 {get;set;}
		// From : "\"Orb\""
		protected System.String Parameter1 {get;set;}
		// From : "\"ORB_GVG\""
		protected System.String Parameter2 {get;set;}
		// From : "[-150.0,495.0]"
		protected System.Single[] Parameter3 {get;set;}
		// From : "Original \"hY19asSLa0qhzsm8iMr/5g==\" : Converted : [104,89,49,57,97,115,83,76,97,48,113,104,122,115,109,56,105,77,114,47,53,103,61,61]"
		protected System.Int16[] Parameter4 {get;set;}
		// From : "101"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewOrbObject(Dictionary<byte,object> data) : base(EventCodes.NewOrbObject,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	public class GEventRandomDungeonPositionInfo : BaseEvent
	{
		// From : "937169"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[304.0,22.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "\"SHARED_RANDOM_EXIT_10x10_PORTAL_SOLO\""
		protected System.String Parameter3 { get; set; }
		// From : "true"
		protected System.Boolean Parameter4 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 { get; set; }
		// From : "297"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRandomDungeonPositionInfo(Dictionary<byte, object> data) : base(EventCodes.RandomDungeonPositionInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToBoolean();
			if (SafeExtract(6) != null)
				Parameter6 = SafeExtract(6).ToBoolean();
			else
				Parameter6 = false; //TODO: check
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	public class GEventNewRandomDungeonExit : BaseEvent
	{
		// From : "937169"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[304.0,22.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "\"SHARED_RANDOM_EXIT_10x10_PORTAL_SOLO\""
		protected System.String Parameter3 { get; set; }
		// From : "true"
		protected System.Boolean Parameter4 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 { get; set; }
		// From : "297"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewRandomDungeonExit(Dictionary<byte, object> data) : base(EventCodes.NewRandomDungeonExit, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToBoolean();
			if (SafeExtract(6) != null)
				Parameter6 = SafeExtract(6).ToBoolean();
			else
				Parameter6 = false; //TODO: check
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewOutpostObject : BaseEvent
	{
		// From : "1103720"
		protected System.Int64 Parameter0 {get;set;}
		// From : "0"
		protected System.Int64 Parameter1 {get;set;}
		// From : "333"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewOutpostObject(Dictionary<byte,object> data) : base(EventCodes.NewOutpostObject,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewRealEstate : BaseEvent
	{
		// From : "1350"
		protected System.Int64 Parameter0 {get;set;}
		// From : "Original \"UesXpPNA1kmYst57KNk9CA==\" : Converted : [85,101,115,88,112,80,78,65,49,107,109,89,115,116,53,55,75,78,107,57,67,65,61,61]"
		protected System.Int16[] Parameter1 {get;set;}
		// From : "[-95.0,-55.0]"
		protected System.Single[] Parameter2 {get;set;}
		// From : "[20.0,20.0]"
		protected System.Single[] Parameter3 {get;set;}
		// From : "Original \"cnWsKrlHfkO5BNoxRbgrSQ==\" : Converted : [99,110,87,115,75,114,108,72,102,107,79,53,66,78,111,120,82,98,103,114,83,81,61,61]"
		protected System.Int16[] Parameter4 {get;set;}
		// From : "\"Vaaled\""
		protected System.String Parameter5 {get;set;}
		// From : "Original \"MDOZuqL/pEqfYEPJou9mSA==\" : Converted : [77,68,79,90,117,113,76,47,112,69,113,102,89,69,80,74,111,117,57,109,83,65,61,61]"
		protected System.Int16[] Parameter6 {get;set;}
		// From : "0"
		protected System.Int64 Parameter7 {get;set;}
		// From : "\"Elevate\""
		protected System.String Parameter9 {get;set;}
		// From : "-10000"
		protected System.Int64 Parameter10 {get;set;}
		// From : "1360000000000"
		protected System.Int64 Parameter11 {get;set;}
		// From : "636359039105246255"
		protected System.Int64 Parameter12 {get;set;}
		// From : "176"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewRealEstate(Dictionary<byte,object> data) : base(EventCodes.NewRealEstate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter5 = SafeExtract(5).ToString();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter9 = SafeExtract(9).ToString();
			Parameter10 = SafeExtract(10).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewSilverObject : BaseEvent
	{
		// From : "1097138"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1047586"
		protected System.Int64 Parameter1 {get;set;}
		// From : "true"
		protected System.Boolean Parameter2 {get;set;}
		// From : "637301928051271751"
		protected System.Int64 Parameter3 {get;set;}
		// From : "Original \"tqc8LsivYEqYXgyn5OFRUg==\" : Converted : [116,113,99,56,76,115,105,118,89,69,113,89,88,103,121,110,53,79,70,82,85,103,61,61]"
		protected System.Int16[] Parameter4 {get;set;}
		// From : "[207.5378,129.49432]"
		protected System.Single[] Parameter5 {get;set;}
		// From : "37"
		protected System.Int64 Parameter6 {get;set;}
		// From : "true"
		protected System.Boolean Parameter8 {get;set;}
		// From : "0"
		protected System.Int64 Parameter13 {get;set;}
		// From : "0"
		protected System.Int64 Parameter14 {get;set;}
		// From : "34"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSilverObject(Dictionary<byte,object> data) : base(EventCodes.NewSilverObject,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToBoolean();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter5 = SafeExtract(5).ToFloatArray();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter8 = SafeExtract(8).ToBoolean();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewSimpleHarvestableObjectList : BaseEvent
	{
		// From : "[4980,5004,7985,5021,7943,4983,5001,4981,5002,5003,5018,6730,5017]"
				protected System.Int32[] Parameter0 { get; set; }
		// From : "Original \"AAA=\" : Converted : \"QUFBPQ==\""
		protected System.Byte[] Parameter1 { get; set; }
		// From : "Original \"AQE=\" : Converted : \"QVFFPQ==\""
		protected System.Byte[] Parameter2 { get; set; }
		// From : "[237.5,85.5,235.5,84.5]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "Original \"AgI=\" : Converted : \"QWdJPQ==\""
		protected System.Byte[] Parameter4 { get; set; }
		// From : "32"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSimpleHarvestableObjectList(Dictionary<byte,object> data) : base(EventCodes.NewSimpleHarvestableObjectList,data)
		{
			try
			{
				List<int> a0 = new List<int>();
				if (data[0].GetType() == typeof(System.Byte[]))
				{
					System.Byte[] typeListByte = (System.Byte[])data[0]; //list of types
					foreach (System.Byte b in typeListByte)
					{
						a0.Add(b);
					}
				}
				else if (data[0].GetType() == typeof(System.Int16[]))
				{
					System.Int16[] typeListByte = (System.Int16[])data[0]; //list of types
					foreach (System.Int16 b in typeListByte)
					{
						a0.Add(b);
					}
				}
				else
				{
				}
				Parameter0 = a0.ToArray();

			}
			catch { Parameter0 = null; }
			Parameter1 = SafeExtract(1).ToByteArray(); 
			Parameter2 = SafeExtract(2).ToByteArray();
			var aa = (SafeExtract(3));
			if (SafeExtract(3) == null || SafeExtract(3).GetType() == typeof(System.Single))
				Parameter3 = new float[0];
			else
				Parameter3 = SafeExtract(3).ToFloatArray();
			if (SafeExtract(4) == null) { Parameter4 = null; } else { Parameter4 = SafeExtract(4).ToByteArray(); }
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewSimpleItem : BaseEvent
	{
		// From : "1103822"
		protected System.Int64 Parameter0 {get;set;}
		// From : "618"
		protected System.Int64 Parameter1 {get;set;}
		// From : "2"
		protected System.Int64 Parameter2 {get;set;}
		// From : "27"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSimpleItem(Dictionary<byte,object> data) : base(EventCodes.NewSimpleItem,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewSpellEffectArea : BaseEvent
	{
		// From : "1104697"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1100624"
		protected System.Int64 Parameter1 {get;set;}
		// From : "[341.17813,11.138637]"
		protected System.Single[] Parameter2 {get;set;}
		// From : "174.08977"
		protected System.Double Parameter3 {get;set;}
		// From : "1278"
		protected System.Int64 Parameter4 {get;set;}
		// From : "0"
		protected System.Int64 Parameter5 {get;set;}
		// From : "46694917"
		protected System.Int64 Parameter6 {get;set;}
		// From : "46695417"
		protected System.Int64 Parameter7 {get;set;}
		// From : "8"
		protected System.Int64 Parameter8 {get;set;}
		// From : "0"
		protected System.Int64 Parameter9 {get;set;}
		// From : "103"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSpellEffectArea(Dictionary<byte,object> data) : base(EventCodes.NewSpellEffectArea,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewTravelpoint : BaseEvent
	{
		// From : "1103399"
		protected System.Int64 Parameter0 {get;set;}
		// From : "197"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewTravelpoint(Dictionary<byte,object> data) : base(EventCodes.NewTravelpoint,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewTreasureChest : BaseEvent
	{
		// From : "6163"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[-225.0,-235.0]"
		protected System.Single[] Parameter1 {get;set;}
		// From : "\"9b28a3b5-64ef-4cf3-81dd-46ff902947dd@1211\""
		protected System.String Parameter2 {get;set;}
		// From : "0"
		protected System.Int64 Parameter3 {get;set;}
		// From : "107"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewTreasureChest(Dictionary<byte,object> data) : base(EventCodes.NewTreasureChest,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventNewUnreadMails : BaseEvent
	{
		// From : "0"
		protected System.Int64 Parameter2 {get;set;}
		// From : "186"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewUnreadMails(Dictionary<byte,object> data) : base(EventCodes.NewUnreadMails,data)
		{
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventObserveStart : BaseEvent
	{
		// From : "100"
		protected System.Int64 Parameter0 {get;set;}
		// From : "302"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventObserveStart(Dictionary<byte,object> data) : base(EventCodes.ObserveStart,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventOtherGrabbedLoot : BaseEvent
	{
		// From : "953689"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"JokerCRI\""
		protected System.String Parameter2 { get; set; }
		// From : "true"
		protected System.Boolean Parameter3 { get; set; }
		// From : "598500"
		protected System.Int64 Parameter5 { get; set; }
		// From : "252"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventOtherGrabbedLoot(Dictionary<byte,object> data) : base(EventCodes.OtherGrabbedLoot,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToBoolean();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventOutpostClaimed : BaseEvent
	{
		// From : "6500"
		protected System.Int64 Parameter0 {get;set;}
		// From : "2"
		protected System.Int64 Parameter1 {get;set;}
		// From : "2"
		protected System.Int64 Parameter2 {get;set;}
		// From : "2"
		protected System.Int64 Parameter3 {get;set;}
		// From : "9223372036854775807"
		protected System.Int64 Parameter4 {get;set;}
		// From : "2"
		protected System.Int64 Parameter5 {get;set;}
		// From : "10000000"
		protected System.Int64 Parameter6 {get;set;}
		// From : "10000000"
		protected System.Int64 Parameter7 {get;set;}
		// From : "637301468637396552"
		protected System.Int64 Parameter8 {get;set;}
		// From : "335"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventOutpostClaimed(Dictionary<byte,object> data) : base(EventCodes.OutpostClaimed,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventOutpostUpdate : BaseEvent
	{
		// From : "6500"
		protected System.Int64 Parameter0 {get;set;}
		// From : "\"FOREST_RED_FACTION_WARFARE_OUTPOST_TOWER\""
		protected System.String Parameter1 {get;set;}
		// From : "\"OUTPOST_FOREST\""
		protected System.String Parameter2 {get;set;}
		// From : "[84.0,246.0]"
		protected System.Single[] Parameter3 {get;set;}
		// From : "2"
		protected System.Int64 Parameter4 {get;set;}
		// From : "2"
		protected System.Int64 Parameter5 {get;set;}
		// From : "9223372036854775807"
		protected System.Int64 Parameter6 {get;set;}
		// From : "334"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventOutpostUpdate(Dictionary<byte,object> data) : base(EventCodes.OutpostUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventOverChargeStatus : BaseEvent
	{
		// From : "2761847"
		protected System.Int64 Parameter0 {get;set;}
		// From : "4"
		protected System.Int64 Parameter1 {get;set;}
		// From : "2"
		protected System.Int64 Parameter2 {get;set;}
		// From : "338"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventOverChargeStatus(Dictionary<byte,object> data) : base(EventCodes.OverChargeStatus,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventPartyFinderFullUpdate : BaseEvent
	{
		// From : "1103812"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[-9223372036854775808,-9223372036854775808,-9223372036854775808,-9223372036854775808,-9223372036854775808]"
		protected System.Int64[] Parameter2 {get;set;}
		// From : "339"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyFinderFullUpdate(Dictionary<byte,object> data) : base(EventCodes.PartyFinderFullUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventQuestProgressInfo : BaseEvent
	{
		// From : "1103812"
		protected System.Int64 Parameter0 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter1 {get;set;}
		// From : "\"\""
		protected System.String Parameter3 {get;set;}
		// From : "0"
		protected System.Int64 Parameter4 {get;set;}
		// From : "255"
		protected System.Int64 Parameter6 {get;set;}
		// From : "-9223372036854775808"
		protected System.Int64 Parameter7 {get;set;}
		// From : "1"
		protected System.Int64 Parameter8 {get;set;}
		// From : "\"\""
		protected System.String Parameter9 {get;set;}
		// From : "237"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventQuestProgressInfo(Dictionary<byte,object> data) : base(EventCodes.QuestProgressInfo,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter9 = SafeExtract(9).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventRegenerationEnergyChanged : BaseEvent
	{
		// From : "1103399"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46670545"
		protected System.Int64 Parameter1 {get;set;}
		// From : "142.0"
		protected System.Double Parameter2 {get;set;}
		// From : "163.0"
		protected System.Double Parameter3 {get;set;}
		// From : "2.1616275"
		protected System.Double Parameter4 {get;set;}
		// From : "637301931523837279"
		protected System.Int64 Parameter5 {get;set;}
		// From : "82"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationEnergyChanged(Dictionary<byte,object> data) : base(EventCodes.RegenerationEnergyChanged,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventRegenerationHealthChanged : BaseEvent
	{
		// From : "1103399"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46657028"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1177.0"
		protected System.Double Parameter2 {get;set;}
		// From : "1429.0"
		protected System.Double Parameter3 {get;set;}
		// From : "35.730476"
		protected System.Double Parameter4 {get;set;}
		// From : "637301931388663743"
		protected System.Int64 Parameter5 {get;set;}
		// From : "81"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationHealthChanged(Dictionary<byte,object> data) : base(EventCodes.RegenerationHealthChanged,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventRegenerationHealthEnergyComboChanged : BaseEvent
	{
		// From : "1099520"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46575095"
		protected System.Int64 Parameter1 {get;set;}
		// From : "685.0"
		protected System.Double Parameter2 {get;set;}
		// From : "685.0"
		protected System.Double Parameter3 {get;set;}
		// From : "637301930569333335"
		protected System.Int64 Parameter5 {get;set;}
		// From : "112.0"
		protected System.Double Parameter6 {get;set;}
		// From : "112.0"
		protected System.Double Parameter7 {get;set;}
		// From : "3.0"
		protected System.Double Parameter8 {get;set;}
		// From : "637301930569333335"
		protected System.Int64 Parameter9 {get;set;}
		// From : "85"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationHealthEnergyComboChanged(Dictionary<byte,object> data) : base(EventCodes.RegenerationHealthEnergyComboChanged,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter8 = SafeExtract(8).ToDouble();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventRegenerationMountHealthChanged : BaseEvent
	{
		// From : "1098669"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46649950"
		protected System.Int64 Parameter1 {get;set;}
		// From : "845.0"
		protected System.Double Parameter2 {get;set;}
		// From : "845.0"
		protected System.Double Parameter3 {get;set;}
		// From : "8.45"
		protected System.Double Parameter4 {get;set;}
		// From : "637301931317885745"
		protected System.Int64 Parameter5 {get;set;}
		// From : "83"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationMountHealthChanged(Dictionary<byte,object> data) : base(EventCodes.RegenerationMountHealthChanged,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventRegenerationPlayerComboChanged : BaseEvent
	{
		// From : "1103812"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46649116"
		protected System.Int64 Parameter1 {get;set;}
		// From : "2022.0"
		protected System.Double Parameter2 {get;set;}
		// From : "2022.0"
		protected System.Double Parameter3 {get;set;}
		// From : "46.497334"
		protected System.Double Parameter4 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter5 {get;set;}
		// From : "256.0"
		protected System.Double Parameter6 {get;set;}
		// From : "256.0"
		protected System.Double Parameter7 {get;set;}
		// From : "3.5235536"
		protected System.Double Parameter8 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter9 {get;set;}
		// From : "669.0"
		protected System.Double Parameter11 {get;set;}
		// From : "6.69"
		protected System.Double Parameter12 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter13 {get;set;}
		// From : "86"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationPlayerComboChanged(Dictionary<byte,object> data) : base(EventCodes.RegenerationPlayerComboChanged,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter8 = SafeExtract(8).ToDouble();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter11 = SafeExtract(11).ToDouble();
			Parameter12 = SafeExtract(12).ToDouble();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventResurrectionOffer : BaseEvent
	{
		// From : "[\"basico\",\"bardo\",\"fama\",\"cura\",\"@CHAT_SETTINGS_TAB_NAME_PARTY\",\"@CHAT_SETTINGS_TAB_NAME_ALLIANCE\",\"loot\",\"ss\",\"@CHAT_SETTINGS_TAB_NAME_GUILD\"]"
		protected System.String[] Parameter0 {get;set;}
		// From : "[[17,18,21,23,19],[16,13,20,23,0],[35],[30],[19,21],[18,21],[32,33],[29,25],[17,21]]"
		protected System.Int64[][] Parameter1 {get;set;}
		// From : "[false,false,false,false,true,true,false,false,true]"
		protected System.Boolean[] Parameter2 {get;set;}
		// From : "[false,false,false,false,false,false,false,false,false]"
		protected System.Boolean[] Parameter3 {get;set;}
		// From : "202"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventResurrectionOffer(Dictionary<byte,object> data) : base(EventCodes.ResurrectionOffer,data)
		{
			Parameter0 = SafeExtract(0).ToStringArray();
			Parameter1 = SafeExtract(1).ToInt2DArray();
			Parameter2 = SafeExtract(2).ToBooleanArray();
			Parameter3 = SafeExtract(3).ToBooleanArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventSiegeCampClaimStart : BaseEvent
	{
		// From : "2760941"
		protected System.Int64 Parameter0 {get;set;}
		// From : "\"Explode5\""
		protected System.String Parameter2 {get;set;}
		// From : "true"
		protected System.Boolean Parameter3 {get;set;}
		// From : "10000000"
		protected System.Int64 Parameter5 {get;set;}
		// From : "255"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventSiegeCampClaimStart(Dictionary<byte,object> data) : base(EventCodes.SiegeCampClaimStart,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToBoolean();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventTakeSilver : BaseEvent
	{
		// From : "2760941"
		protected System.Int64 Parameter0 {get;set;}
		// From : "46615470"
		protected System.Int64 Parameter1 {get;set;}
		// From : "2760941"
		protected System.Int64 Parameter2 {get;set;}
		// From : "10000000"
		protected System.Int64 Parameter3 {get;set;}
		// From : "0"
		protected System.Int64 Parameter4 {get;set;}
		// From : "0"
		protected System.Int64 Parameter5 {get;set;}
		// From : "0"
		protected System.Int64 Parameter6 {get;set;}
		// From : "52"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTakeSilver(Dictionary<byte,object> data) : base(EventCodes.TakeSilver,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventTimeSync : BaseEvent
	{
		// From : "46702871"
		protected System.Int64 Parameter0 {get;set;}
		// From : "1986474254"
		protected System.Int64 Parameter1 {get;set;}
		// From : "148"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTimeSync(Dictionary<byte,object> data) : base(EventCodes.TimeSync,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventTutorialUpdate : BaseEvent
	{
		// From : "\"GVG_SEASON_08#3\""
		protected System.String Parameter0 {get;set;}
		// From : "\"SCHEMA_01\""
		protected System.String Parameter1 {get;set;}
		// From : "\"GUILDSYMBOL_RED_ARMY\""
		protected System.String Parameter2 {get;set;}
		// From : "8"
		protected System.Int64 Parameter3 {get;set;}
		// From : "2"
		protected System.Int64 Parameter4 {get;set;}
		// From : "15775037"
		protected System.Int64 Parameter5 {get;set;}
		// From : "0.7"
		protected System.Double Parameter6 {get;set;}
		// From : "-0.15"
		protected System.Double Parameter7 {get;set;}
		// From : "true"
		protected System.Boolean Parameter8 {get;set;}
		// From : "357"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTutorialUpdate(Dictionary<byte,object> data) : base(EventCodes.TutorialUpdate,data)
		{
			Parameter0 = SafeExtract(0).ToString();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter8 = SafeExtract(8).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventUpdateFame : BaseEvent
	{
		// From : "961422"
		protected System.Int64 Parameter0 {get;set;}
		// From : "713417528329"
		protected System.Int64 Parameter1 {get;set;}
		// From : "1580000"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1"
		protected System.Int64 Parameter3 {get;set;}
		// From : "true"
		protected System.Boolean Parameter4 {get;set;}
		// From : "1.0"
		protected System.Double Parameter6 {get;set;}
		// From : "0"
		protected System.Int64 Parameter8 {get;set;}
		// From : "73"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateFame(Dictionary<byte,object> data) : base(EventCodes.UpdateFame,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			//Parameter4 = SafeExtract(4).ToBoolean();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventUpdateHome : BaseEvent
	{
		// From : "4966"
		protected System.Int64 Parameter0 {get;set;}
		// From : "Original \"NrGolDnKaUii2N+LyyQqXw==\" : Converted : [78,114,71,111,108,68,110,75,97,85,105,105,50,78,43,76,121,121,81,113,88,119,61,61]"
		protected System.Int16[] Parameter1 {get;set;}
		// From : "[392.5,-0.5]"
		protected System.Single[] Parameter2 {get;set;}
		// From : "200"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateHome(Dictionary<byte,object> data) : base(EventCodes.UpdateHome,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventUpdateMoney : BaseEvent
	{
		// From : "961422"
		protected System.Int64 Parameter0 {get;set;}
		// From : "21593085734"
		protected System.Int64 Parameter1 {get;set;}
		// From : "72"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateMoney(Dictionary<byte,object> data) : base(EventCodes.UpdateMoney,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventUpdateReSpecPoints : BaseEvent
	{
		// From : "[0,7652613960,0,0,0]"
		protected System.Int64[] Parameter0 {get;set;}
		// From : "75"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateReSpecPoints(Dictionary<byte,object> data) : base(EventCodes.UpdateReSpecPoints,data)
		{
			Parameter0 = SafeExtract(0).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventUpdateUnlockedAvatarRings : BaseEvent
	{
		// From : "Original \"ERI=\" : Converted : \"RVJJPQ==\""
		protected System.Byte[] Parameter0 {get;set;}
		// From : "207"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateUnlockedAvatarRings(Dictionary<byte,object> data) : base(EventCodes.UpdateUnlockedAvatarRings,data)
		{
			Parameter0 = SafeExtract(0).ToByteArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventUpdateUnlockedBuildings : BaseEvent
	{
		// From : "Original \"FhcMDRgZGgs=\" : Converted : \"RmhjTURSZ1pHZ3M9\""
		protected System.Byte[] Parameter0 {get;set;}
		// From : "208"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateUnlockedBuildings(Dictionary<byte,object> data) : base(EventCodes.UpdateUnlockedBuildings,data)
		{
			Parameter0 = SafeExtract(0).ToByteArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventUpdateUnlockedGuildLogos : BaseEvent
	{
		// From : "1068506"
		protected System.Int64 Parameter0 {get;set;}
		// From : "[4314,0,2655,3105,3061,1786,1566,1895,0,0]"
		protected System.Int64[] Parameter1 {get;set;}
		// From : "205"
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateUnlockedGuildLogos(Dictionary<byte,object> data) : base(EventCodes.UpdateUnlockedGuildLogos,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//OperationRequest | BaseCode - 1
	public class GOperationRequestPing : BaseOpRequest
	{
		// From : "18"
		protected System.Int64 Parameter0 {get;set;}
		// From : "3"
		protected System.Int64 Parameter1 {get;set;}
		// From : "23"
		protected System.Int64 Parameter2 {get;set;}
		// From : "1049088"
		protected System.Int64 Parameter3 {get;set;}
		// From : "\"1366x768\""
		protected System.String Parameter4 {get;set;}
		// From : "300"
		protected System.Int64 Parameter253 {get;set;}
		protected GOperationRequestPing(Dictionary<byte,object> data) : base(OperationCodes.Ping,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter253 = SafeExtract(253).ToInt();
		}
	}

	//OperationResponse | BaseCode - 1
	public class GOperationResponsePing : BaseOpResponse
	{
		// From : "1103812"
		protected System.Int64 Parameter0 {get;set;}
		// From : "Original \"CcxeEDirHk+YRdRgo8iwPg==\" : Converted : [67,99,120,101,69,68,105,114,72,107,43,89,82,100,82,103,111,56,105,119,80,103,61,61]"
		protected System.Int16[] Parameter1 {get;set;}
		// From : "\"lucianoPereira\""
		protected System.String Parameter2 {get;set;}
		// From : "29"
		protected System.Int64 Parameter3 {get;set;}
		// From : "26"
		protected System.Int64 Parameter4 {get;set;}
		// From : "Original \"AgAAAAQ=\" : Converted : \"QWdBQUFBUT0=\""
		protected System.Byte[] Parameter6 {get;set;}
		// From : "Original \"AAIKAAQ=\" : Converted : \"QUFJS0FBUT0=\""
		protected System.Byte[] Parameter7 {get;set;}
		// From : "\"1211\""
		protected System.String Parameter8 {get;set;}
		// From : "[370.0,1.9669533E-06]"
		protected System.Single[] Parameter9 {get;set;}
		// From : "271.27304"
		protected System.Double Parameter10 {get;set;}
		// From : "2022.0"
		protected System.Double Parameter11 {get;set;}
		// From : "2022.0"
		protected System.Double Parameter12 {get;set;}
		// From : "46.497334"
		protected System.Double Parameter13 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter14 {get;set;}
		// From : "256.0"
		protected System.Double Parameter15 {get;set;}
		// From : "256.0"
		protected System.Double Parameter16 {get;set;}
		// From : "3.5235536"
		protected System.Double Parameter17 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter18 {get;set;}
		// From : "669.0"
		protected System.Double Parameter19 {get;set;}
		// From : "669.0"
		protected System.Double Parameter20 {get;set;}
		// From : "6.69"
		protected System.Double Parameter21 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter22 {get;set;}
		// From : "30000.0"
		protected System.Double Parameter23 {get;set;}
		// From : "30000.0"
		protected System.Double Parameter24 {get;set;}
		// From : "637092885816470000"
		protected System.Int64 Parameter26 {get;set;}
		// From : "21593085734"
		protected System.Int64 Parameter28 {get;set;}
		// From : "0"
		protected System.Int64 Parameter29 {get;set;}
		// From : "713417528329"
		protected System.Int64 Parameter30 {get;set;}
		// From : "5610000"
		protected System.Int64 Parameter32 {get;set;}
		// From : "637092885816470000"
		protected System.Int64 Parameter33 {get;set;}
		// From : "9999990000"
		protected System.Int64 Parameter34 {get;set;}
		// From : "0"
		protected System.Int64 Parameter35 {get;set;}
		// From : "-998.72766"
		protected System.Double Parameter36 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter37 {get;set;}
		// From : "[0,7652613960,0,0,0]"
		protected System.Int64[] Parameter38 {get;set;}
		// From : "5"
		protected System.Int64 Parameter39 {get;set;}
		// From : "\"\""
		protected System.String Parameter40 {get;set;}
		// From : "\"\""
		protected System.String Parameter41 {get;set;}
		// From : "0"
		protected System.Int64 Parameter42 {get;set;}
		// From : "true"
		protected System.Boolean Parameter43 {get;set;}
		// From : "Original \"t3SJXX7drUiefmaB6OL3YA==\" : Converted : [116,51,83,74,88,88,55,100,114,85,105,101,102,109,97,66,54,79,76,51,89,65,61,61]"
		protected System.Int16[] Parameter44 {get;set;}
		// From : "[1103818,0,1103814,1103813,1103816,1103819,1103817,1103815,1103821,1103820]"
		protected System.Int64[] Parameter45 {get;set;}
		// From : "Original \"5gLc6K9WLk+4JYrAQmgLug==\" : Converted : [53,103,76,99,54,75,57,87,76,107,43,52,74,89,114,65,81,109,103,76,117,103,61,61]"
		protected System.Int16[] Parameter47 {get;set;}
		// From : "[1103822,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]"
		protected System.Int64[] Parameter48 {get;set;}
		// From : "0"
		protected System.Int64 Parameter50 {get;set;}
		// From : "637259754200306033"
		protected System.Int64 Parameter52 {get;set;}
		// From : "[8.254115,-5.665108]"
		protected System.Single[] Parameter53 {get;set;}
		// From : "\"2303\""
		protected System.String Parameter54 {get;set;}
		// From : "\"1000\""
		protected System.String Parameter55 {get;set;}
		// From : "637255281752188356"
		protected System.Int64 Parameter56 {get;set;}
		// From : "[0.0,0.0]"
		protected System.Single[] Parameter57 {get;set;}
		// From : "\"\""
		protected System.String Parameter59 {get;set;}
		// From : "637301931309546540"
		protected System.Int64 Parameter60 {get;set;}
		// From : "1986420504"
		protected System.Int64 Parameter61 {get;set;}
		// From : "\"\""
		protected System.String Parameter69 {get;set;}
		// From : "\"\""
		protected System.String Parameter71 {get;set;}
		// From : "28300000"
		protected System.Int64 Parameter73 {get;set;}
		// From : "\"\""
		protected System.String Parameter76 {get;set;}
		// From : "637092885816470000"
		protected System.Int64 Parameter77 {get;set;}
		// From : "156"
		protected System.Int64 Parameter78 {get;set;}
		// From : "636954118016942026"
		protected System.Int64 Parameter79 {get;set;}
		// From : "Original \"AAE=\" : Converted : \"QUFFPQ==\""
		protected System.Byte[] Parameter80 {get;set;}
		// From : "[637068740665370121,637067799201060163]"
		protected System.Int64[] Parameter81 {get;set;}
		// From : "Original \"AQM=\" : Converted : \"QVFNPQ==\""
		protected System.Byte[] Parameter82 {get;set;}
		// From : "true"
		protected System.Boolean Parameter84 {get;set;}
		// From : "true"
		protected System.Boolean Parameter86 {get;set;}
		// From : "637301931230960496"
		protected System.Int64 Parameter87 {get;set;}
		// From : "637301859206843436"
		protected System.Int64 Parameter88 {get;set;}
		// From : "4"
		protected System.Int64 Parameter89 {get;set;}
		// From : "{\"1\":110160000,\"4\":20950000,\"5\":1080000}"
		protected System.Object Parameter90 {get;set;}
		// From : "[0,1868593500,1200407500,0,137080000]"
		protected System.Int64[] Parameter91 {get;set;}
		// From : "188547"
		protected System.Int64 Parameter92 {get;set;}
		// From : "[-1,-1,-1,-1,-1,-1,-1]"
		protected System.Int64[] Parameter93 {get;set;}
		// From : "-1"
		protected System.Int64 Parameter94 {get;set;}
		// From : "2"
		protected System.Int64 Parameter253 {get;set;}
		protected GOperationResponsePing(Dictionary<byte,object> data) : base(OperationCodes.Ping,data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter6 = SafeExtract(6).ToByteArray();
			Parameter7 = SafeExtract(7).ToByteArray();
			Parameter8 = SafeExtract(8).ToString();
			Parameter9 = SafeExtract(9).ToFloatArray();
			Parameter10 = SafeExtract(10).ToDouble();
			Parameter11 = SafeExtract(11).ToDouble();
			Parameter12 = SafeExtract(12).ToDouble();
			Parameter13 = SafeExtract(13).ToDouble();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToDouble();
			Parameter16 = SafeExtract(16).ToDouble();
			Parameter17 = SafeExtract(17).ToDouble();
			Parameter18 = SafeExtract(18).ToInt();
			Parameter19 = SafeExtract(19).ToDouble();
			Parameter20 = SafeExtract(20).ToDouble();
			Parameter21 = SafeExtract(21).ToDouble();
			Parameter22 = SafeExtract(22).ToInt();
			Parameter23 = SafeExtract(23).ToDouble();
			Parameter24 = SafeExtract(24).ToDouble();
			Parameter26 = SafeExtract(26).ToInt();
			Parameter28 = SafeExtract(28).ToInt();
			Parameter29 = SafeExtract(29).ToInt();
			Parameter30 = SafeExtract(30).ToInt();
			Parameter32 = SafeExtract(32).ToInt();
			Parameter33 = SafeExtract(33).ToInt();
			Parameter34 = SafeExtract(34).ToInt();
			Parameter35 = SafeExtract(35).ToInt();
			Parameter36 = SafeExtract(36).ToDouble();
			Parameter37 = SafeExtract(37).ToInt();
			Parameter38 = SafeExtract(38).ToIntArray();
			Parameter39 = SafeExtract(39).ToInt();
			Parameter40 = SafeExtract(40).ToString();
			Parameter41 = SafeExtract(41).ToString();
			Parameter42 = SafeExtract(42).ToInt();
			Parameter43 = SafeExtract(43).ToBoolean();
			Parameter44 = SafeExtract(44).ToInt16Array();
			Parameter45 = SafeExtract(45).ToIntArray();
			Parameter47 = SafeExtract(47).ToInt16Array();
			Parameter48 = SafeExtract(48).ToIntArray();
			Parameter50 = SafeExtract(50).ToInt();
			Parameter52 = SafeExtract(52).ToInt();
			Parameter53 = SafeExtract(53).ToFloatArray();
			Parameter54 = SafeExtract(54).ToString();
			Parameter55 = SafeExtract(55).ToString();
			Parameter56 = SafeExtract(56).ToInt();
			Parameter57 = SafeExtract(57).ToFloatArray();
			Parameter59 = SafeExtract(59).ToString();
			Parameter60 = SafeExtract(60).ToInt();
			Parameter61 = SafeExtract(61).ToInt();
			Parameter69 = SafeExtract(69).ToString();
			Parameter71 = SafeExtract(71).ToString();
			Parameter73 = SafeExtract(73).ToInt();
			Parameter76 = SafeExtract(76).ToString();
			Parameter77 = SafeExtract(77).ToInt();
			Parameter78 = SafeExtract(78).ToInt();
			Parameter79 = SafeExtract(79).ToInt();
			Parameter80 = SafeExtract(80).ToByteArray();
			Parameter81 = SafeExtract(81).ToIntArray();
			Parameter82 = SafeExtract(82).ToByteArray();
			Parameter84 = SafeExtract(84).ToBoolean();
			Parameter86 = SafeExtract(86).ToBoolean();
			Parameter87 = SafeExtract(87).ToInt();
			Parameter88 = SafeExtract(88).ToInt();
			Parameter89 = SafeExtract(89).ToInt();
			Parameter90 = SafeExtract(90);
			Parameter91 = SafeExtract(91).ToIntArray();
			Parameter92 = SafeExtract(92).ToInt();
			Parameter93 = SafeExtract(93).ToIntArray();
			Parameter94 = SafeExtract(94).ToInt();
			Parameter253 = SafeExtract(253).ToInt();
		}
	}
}
