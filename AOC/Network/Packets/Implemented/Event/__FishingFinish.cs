﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __FishingFinish : GEventFishingFinish
    {
        public __FishingFinish(Dictionary<byte, object> data) : base(data)
        {
            F = Parameter1;
            
        }

        public bool F { get; }
       
    }
}