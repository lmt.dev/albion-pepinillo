﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __MobChangeState : GEventMobChangeState
    {
        public __MobChangeState(Dictionary<byte, object> data) : base(data)
        {
            MobId = Parameter0;
            EnchantmentLevel = Parameter1;
        }

        public long MobId { get; }
        public float EnchantmentLevel { get; set; }
    }
}