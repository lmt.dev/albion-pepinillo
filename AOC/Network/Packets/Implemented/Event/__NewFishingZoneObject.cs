﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __NewFishingZoneObject : GEventNewFishingZoneObject
    {
        public __NewFishingZoneObject(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            X = Parameter1[0];
            Y = Parameter1[1];
            Amount = Parameter2;
            A = Parameter3.ToString();
            Type = Parameter4;

        }

        public long Id { get; set; }
        public float X { get; }
        public float Y { get; }
        public long Amount { get; set; }
        public string A { get; set; }
        public string Type { get; set; }
    }
}