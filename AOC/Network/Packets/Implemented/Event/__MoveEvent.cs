﻿using System;
using System.Collections.Generic;
using System.Text;
using AOC.Network.Data;
using AOC.Network.Packets.Base;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __MoveEvent : BaseEvent
    {
        public __MoveEvent(Dictionary<byte, object> data) : base(EventCodes.Move,data)
        {
            Id = long.Parse(data[0].ToString());
            Data = (Byte[])data[1];
            Loc = new Single[] { BitConverter.ToSingle(Data,9), BitConverter.ToSingle(Data, 13) };
        
        }

        public long Id { get; }
        public Byte[] Data { get; }
        public float[] Loc { get; }
    }
}
