﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __Leave : GEventLeave
    {
        public __Leave(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
        }

        public long Id { get; }
    }
}