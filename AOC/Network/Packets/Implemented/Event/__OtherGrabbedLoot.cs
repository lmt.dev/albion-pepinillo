﻿using System;
using System.Collections.Generic;
using System.Text;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __OtherGrabbedLoot : GEventOtherGrabbedLoot
    {
        public __OtherGrabbedLoot(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            VictimName = data.ContainsKey(2) ? data[2].ToString() : null; //may be null
            LooterName = Parameter2;
            ItemIndex = data.ContainsKey(4) ? (long)data[4] : -1;
            Cantidad = Parameter5;
        }

        public long Id { get; }
        public string VictimName { get; }
        public string LooterName { get; }
        public long ItemIndex { get; }
        public long Cantidad { get; }
    }
}
