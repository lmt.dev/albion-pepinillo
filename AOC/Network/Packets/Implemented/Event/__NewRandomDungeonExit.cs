﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __NewRandomDungeonExit : GEventNewRandomDungeonExit
    {
        public __NewRandomDungeonExit(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            X = Parameter1[0];
            Y = Parameter1[1];
            Name = Parameter3;
            Bandera = Parameter4;
        }

        public long Id { get; }
        public float X { get; set; }
        public float Y { get; set; }
        public string Name { get; }
        public bool Bandera { get; }
    }
}