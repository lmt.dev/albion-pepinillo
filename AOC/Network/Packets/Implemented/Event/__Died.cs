﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __Died : GEventDied
    {
        public __Died(Dictionary<byte, object> data) : base(data)
        {
            X = Parameter0[0];
            Y = Parameter0[1];
            IdVictim = Parameter1;
            Victim = Parameter2;
            IdKiller = Parameter3;
            Killer = Parameter4;
            KillerGuild = Parameter5;
        }

        public float X { get; }
        public float Y { get; }
        public long IdVictim { get; }
        public string Victim { get; }
        public long IdKiller { get; }
        public string Killer { get; }
        public string KillerGuild { get; }
    }
}