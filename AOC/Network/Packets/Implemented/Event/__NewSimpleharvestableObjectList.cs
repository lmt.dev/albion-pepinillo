﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __NewSimpleHarvestableObjectList : GEventNewSimpleHarvestableObjectList
    {
        public __NewSimpleHarvestableObjectList(Dictionary<byte, object> data) : base(data)
        {
            //IdList = Parameter0; //may be Byte[] or Int16[];
            //TypeList = Parameter1;
            //TierList = Parameter2;
            //LocList = Parameter3;
            //SizeList = Parameter4;
        }

        public int[]IdList { get; }
        public byte[] TypeList { get; }
        public byte[] TierList { get; }
        public float[] LocList { get; }
        public byte[] SizeList { get; }
    }
}