﻿using System.Collections.Generic;

namespace AOC.Network.Photon.Protocol16.Core
{
    public class OperationRequest
    {
        public OperationRequest(byte operationCode, Dictionary<byte, object> parameters)
        {
            OperationCode = operationCode;
            Parameters = parameters;
        }

        public byte OperationCode { get; }
        public Dictionary<byte, object> Parameters { get; }
    }
}