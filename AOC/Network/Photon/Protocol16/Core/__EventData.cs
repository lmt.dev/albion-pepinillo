﻿using System.Collections.Generic;

namespace AOC.Network.Photon.Protocol16.Core
{
    public class __EventData
    {
        public __EventData(byte code, Dictionary<byte, object> parameters)
        {
            Code = code;
            Parameters = parameters;
        }

        public byte Code { get; }
        public Dictionary<byte, object> Parameters { get; }
    }
}