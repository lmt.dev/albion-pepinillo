﻿namespace AOC.Network.Photon.Protocol16.Core
{
    public class NumberDeserializer
    {
        public static void Deserialize(out int value, byte[] source, ref int offset)
        {
            var value1 = source[offset] << 24;
            offset++;
            var value2 = value1 | (source[offset] << 16);
            offset++;
            var value3 = value2 | (source[offset] << 8);
            offset++;
            value = value3 | source[offset];
            offset++;
        }

        public static void Deserialize(out short value, byte[] source, ref int offset)
        {
            var value1 = (short) (source[offset] << 8);
            offset++;
            value = (short) (value1 | source[offset]);
            offset++;
        }
    }
}