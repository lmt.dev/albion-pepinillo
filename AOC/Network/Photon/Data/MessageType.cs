﻿namespace AOC.Network.Photon.Data
{
    public enum MessageType
    {
        OperationRequest = 2,
        OperationResponse = 3,
        Event = 4
    }
}