﻿namespace AOC.Network.Photon.Data
{
    public enum CommandType
    {
        Disconnect = 4,
        SendReliable = 6,
        SendUnreliable = 7,
        SendFragment = 8
    }
}