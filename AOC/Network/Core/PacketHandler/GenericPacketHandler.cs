﻿namespace AOC.Network.Core.PacketHandler
{
    public abstract class GenericPacketHandler<T> : IPacketHandler
    {
        private IPacketHandler __nextHandler;

        public IPacketHandler SetNext(IPacketHandler handler)
        {
            __nextHandler = handler;

            return handler;
        }

        public void __Handle(object request)
        {
            if (request is T)
                OnHandle((T) request);
            else if (__nextHandler != null) __Next(request);
        }

        protected internal abstract void OnHandle(T packet);

        protected void __Next(object request)
        {
            __nextHandler?.__Handle(request);
        }
    }
}