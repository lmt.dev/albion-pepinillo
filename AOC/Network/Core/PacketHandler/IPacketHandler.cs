﻿namespace AOC.Network.Core.PacketHandler
{
    public interface IPacketHandler
    {
        IPacketHandler SetNext(IPacketHandler nextHandler);
        void __Handle(object request);
    }
}