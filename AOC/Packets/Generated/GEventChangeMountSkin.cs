using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 148
	public class GEventChangeMountSkin : BaseEvent
	{
		// From : 62821293
		protected System.Int64 Parameter0 {get;set;}
		// From : 431789925
		protected System.Int64 Parameter1 {get;set;}
		// From : 148
		protected System.Int64 Parameter252 {get;set;}
		protected GEventChangeMountSkin(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ChangeMountSkin;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
