using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 221
	public class GEventPartyMarkedObjectsUpdated : BaseEvent
	{
		// From : 21968
		protected System.Int64 Parameter0 {get;set;}
		// From : [0,0,0,0,77362,0]
		protected System.Int64[] Parameter1 {get;set;}
		// From : 221
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyMarkedObjectsUpdated(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyMarkedObjectsUpdated;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
