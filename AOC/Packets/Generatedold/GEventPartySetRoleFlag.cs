using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 223
	public class GEventPartySetRoleFlag : BaseEvent
	{
		// From : -1
		protected System.Int64 Parameter0 {get;set;}
		// From : "atohhloO10SIkaqVDVFfpQ=="
		protected System.String Parameter1 {get;set;}
		// From : 223
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartySetRoleFlag(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartySetRoleFlag;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
