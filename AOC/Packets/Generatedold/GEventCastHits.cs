using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 19
	public class GEventCastHits : BaseEvent
	{
		// From : 941099
		protected System.Int64 Parameter0 {get;set;}
		// From : [9412,9412,9412]
		protected System.Int64[] Parameter1 {get;set;}
		// From : [718,689,703]
		protected System.Int64[] Parameter2 {get;set;}
		// From : "AQEB"
		protected System.String Parameter3 {get;set;}
		// From : "AAAA"
		protected System.String Parameter4 {get;set;}
		// From : 19
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastHits(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CastHits;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToIntArray();
			Parameter2 = data[2].ToIntArray();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
