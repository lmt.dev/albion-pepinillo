using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 143
	public class GEventQuestGiverQuestOffered : BaseEvent
	{
		// From : 1752
		protected System.Int64 Parameter0 {get;set;}
		// From : -1
		protected System.Int64 Parameter2 {get;set;}
		// From : 11
		protected System.Int64 Parameter3 {get;set;}
		// From : ""
		protected System.String Parameter4 {get;set;}
		// From : -9223372036854775808
		protected System.Int64 Parameter6 {get;set;}
		// From : -9223372036854775808
		protected System.Int64 Parameter7 {get;set;}
		// From : 143
		protected System.Int64 Parameter252 {get;set;}
		protected GEventQuestGiverQuestOffered(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.QuestGiverQuestOffered;
			Parameter0 = data[0].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToString();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
