using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 81
	public class GEventPartyFinderUpdate : BaseEvent
	{
		// From : 3
		protected System.Int64 Parameter0 {get;set;}
		// From : 1
		protected System.Int64 Parameter1 {get;set;}
		// From : "CcxeEDirHk+YRdRgo8iwPg=="
		protected System.String Parameter2 {get;set;}
		// From : "Palurdo"
		protected System.String Parameter4 {get;set;}
		// From : ["Palurdo"]
		protected System.String[] Parameter5 {get;set;}
		// From : "HA=="
		protected System.String Parameter6 {get;set;}
		// From : "GQ=="
		protected System.String Parameter7 {get;set;}
		// From : "AA=="
		protected System.String Parameter8 {get;set;}
		// From : [0.0]
		protected System.Single[] Parameter9 {get;set;}
		// From : [[1,0,0]]
		protected System.Int64[][] Parameter10 {get;set;}
		// From : 0
		protected System.Int64 Parameter11 {get;set;}
		// From : 20
		protected System.Int64 Parameter13 {get;set;}
		// From : ""
		protected System.String Parameter15 {get;set;}
		// From : ""
		protected System.String Parameter16 {get;set;}
		// From : 0
		protected System.Int64 Parameter17 {get;set;}
		// From : 337
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyFinderUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyFinderUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToString();
			Parameter4 = data[4].ToString();
			Parameter5 = data[5].ToStringArray();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToFloatArray();
			Parameter10 = data[10].ToInt2DArray();
			Parameter11 = data[11].ToInt();
			Parameter13 = data[13].ToInt();
			Parameter15 = data[15].ToString();
			Parameter16 = data[16].ToString();
			Parameter17 = data[17].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
