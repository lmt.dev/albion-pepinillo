using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 39
	public class GEventNewPortalEntrance : BaseEvent
	{
		// From : 4
		protected System.Int64 Parameter0 {get;set;}
		// From : [-26.0,26.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : "3804ziJEG0CcXcl7nIXqAg=="
		protected System.String Parameter2 {get;set;}
		// From : 295
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewPortalEntrance(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewPortalEntrance;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
