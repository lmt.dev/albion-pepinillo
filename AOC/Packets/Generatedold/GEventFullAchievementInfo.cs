using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 137
	public class GEventFullAchievementInfo : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : [419,379,330,140,406,261,413,97,24,4,414,235,407,0,415,2,75,416,300,380,103,5,417,3,418,76,1,370,360,35,25,70,109,141,37,28,388,72,73,74,110,137,138,50,124,52]
		protected System.Int64[] Parameter1 {get;set;}
		// From : [6,11,16,17,18,19,26,27,29,30,31,32,33,34,36,38,39,40,45,46,47,48,49,51,53,54,55,60,61,62,63,64,65,71,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,98,99,100,101,102,104,105,106,107,108,111,112,113,114,115,116,117,118,119,125,126,127,128,129,130,131,132,133,134,135,136,139,142,152,154,162,164,165,166,167,168,169,174,184,194,199,209,214,262,267,272,273,277,282,283,284,301,305,309,313,317,321,325,331,332,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,361,362,363,364,365,366,367,368,369,371,381,386,389,390,394,400,408,409,410]
		protected System.Int64[] Parameter2 {get;set;}
		// From : "AAAgAAgAEwUBQwsHCQwPFwEAAQEBAAEtPDwAUAEBAQEASz8EHgAfSwkaAAYZAQAFASoDAAAAKgAQAAApAAQDBwk5AEYCKQMIAAIACwAAAAABAAAAAAUAAAAABwAAAwAAAAAAAAAAAAABAAAMCAAAAAAAAAAAAABYBQAMAQAHCBEJYSEAABQAAAAALQAACQAAAAAEAAAAAQAAAAYEAA=="
		protected System.String Parameter3 {get;set;}
		// From : [true,false,true,true,true,false,true,true,true,true,true,true,true,true,true,true,true,false,true,true,true,false,true,true,true,true,true,true,true,true,true,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,false,true,true,false,true,false,false,true,true,true,true,true,false,true,true,true,true,true,true,true,false,true,true,true,true,true,false,true,false,true,true,false,false,false,true,false,false,false,false,true,true,false,true,false,true,false,false,true,false,false,false,true,true,true,true,false,true,true,true,false,false,true,true,true,false,false,true,false,false,false,false,true,false,true,true,false,true,true,true,true,true,true,true,true,true,false,false,true,false,false,true,false,true,false,false,true,false,false,false,false,true,false,true,true,true,false,true,true,true,true,true]
		protected System.Boolean[] Parameter4 {get;set;}
		// From : [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]
		protected System.Boolean[] Parameter5 {get;set;}
		// From : 137
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFullAchievementInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FullAchievementInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToIntArray();
			Parameter2 = data[2].ToIntArray();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToBooleanArray();
			Parameter5 = data[5].ToBooleanArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
