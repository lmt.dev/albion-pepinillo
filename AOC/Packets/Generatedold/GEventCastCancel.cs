using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 14
	public class GEventCastCancel : BaseEvent
	{
		// From : 952078
		protected System.Int64 Parameter0 {get;set;}
		// From : true
		protected System.Boolean Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter2 {get;set;}
		// From : 14
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CastCancel;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToBoolean();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
