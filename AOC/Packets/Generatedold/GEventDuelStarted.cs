using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 169
	public class GEventDuelStarted : BaseEvent
	{
		// From : 2502112
		protected System.Int64 Parameter0 {get;set;}
		// From : 2470439
		protected System.Int64 Parameter1 {get;set;}
		// From : 169
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDuelStarted(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DuelStarted;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
