using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 199
	public class GEventUpdateChatSettings : BaseEvent
	{
		// From : ["basico","bardo","fama","cura","@CHAT_SETTINGS_TAB_NAME_PARTY","@CHAT_SETTINGS_TAB_NAME_ALLIANCE","loot","ss","@CHAT_SETTINGS_TAB_NAME_GUILD"]
		protected System.String[] Parameter0 {get;set;}
		// From : [[15,16,19,21,17],[14,13,18,21,0],[33],[28],[17,19],[16,19],[30,31],[27,23],[15,19]]
		protected System.Int64[][] Parameter1 {get;set;}
		// From : [false,false,false,false,true,true,false,false,true]
		protected System.Boolean[] Parameter2 {get;set;}
		// From : [false,false,false,false,false,false,false,false,false]
		protected System.Boolean[] Parameter3 {get;set;}
		// From : 199
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateChatSettings(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateChatSettings;
			Parameter0 = data[0].ToStringArray();
			Parameter1 = data[1].ToInt2DArray();
			Parameter2 = data[2].ToBooleanArray();
			Parameter3 = data[3].ToBooleanArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
