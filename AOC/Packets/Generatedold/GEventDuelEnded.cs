using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 170
	public class GEventDuelEnded : BaseEvent
	{
		// From : 2456241
		protected System.Int64 Parameter0 {get;set;}
		// From : 3
		protected System.Int64 Parameter1 {get;set;}
		// From : 170
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDuelEnded(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DuelEnded;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
