using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 196
	public class GEventNewIslandAccessPoint : BaseEvent
	{
		// From : 15
		protected System.Int64 Parameter0 {get;set;}
		// From : [-53.0,0.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : 90.0
		protected System.Double Parameter2 {get;set;}
		// From : "8QeoNME66Eah2RhqVrBEPg=="
		protected System.String Parameter3 {get;set;}
		// From : 196
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewIslandAccessPoint(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewIslandAccessPoint;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
