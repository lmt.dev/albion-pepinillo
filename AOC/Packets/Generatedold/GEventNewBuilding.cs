using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 34
	public class GEventNewBuilding : BaseEvent
	{
		// From : 268
		protected System.Int64 Parameter0 {get;set;}
		// From : "+AWVJYXw+U6TWjQDyrymJA=="
		protected System.String Parameter1 {get;set;}
		// From : 592
		protected System.Int64 Parameter2 {get;set;}
		// From : "UNIQUE_FURNITUREITEM_KNIGHT_THRONE_A"
		protected System.String Parameter3 {get;set;}
		// From : [214.0,86.0]
		protected System.Single[] Parameter4 {get;set;}
		// From : 90.0
		protected System.Double Parameter5 {get;set;}
		// From : "CcxeEDirHk+YRdRgo8iwPg=="
		protected System.String Parameter6 {get;set;}
		// From : "n/hYzrG/QEKxooIWbbEiBA=="
		protected System.String Parameter7 {get;set;}
		// From : "System"
		protected System.String Parameter8 {get;set;}
		// From : "Palurdo"
		protected System.String Parameter9 {get;set;}
		// From : 2700000
		protected System.Int64 Parameter16 {get;set;}
		// From : 636971035762547209
		protected System.Int64 Parameter17 {get;set;}
		// From : 637186256926666026
		protected System.Int64 Parameter19 {get;set;}
		// From : -1
		protected System.Int64 Parameter20 {get;set;}
		// From : 637186256926666026
		protected System.Int64 Parameter21 {get;set;}
		// From : 637186256926666026
		protected System.Int64 Parameter23 {get;set;}
		// From : true
		protected System.Boolean Parameter26 {get;set;}
		// From : 0
		protected System.Int64 Parameter30 {get;set;}
		// From : 34
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewBuilding(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewBuilding;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToFloatArray();
			Parameter5 = data[5].ToDouble();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToString();
			Parameter16 = data[16].ToInt();
			Parameter17 = data[17].ToInt();
			Parameter19 = data[19].ToInt();
			Parameter20 = data[20].ToInt();
			Parameter21 = data[21].ToInt();
			Parameter23 = data[23].ToInt();
			Parameter26 = data[26].ToBoolean();
			Parameter30 = data[30].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
