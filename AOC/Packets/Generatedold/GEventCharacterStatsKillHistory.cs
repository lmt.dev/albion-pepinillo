using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 133
	public class GEventCharacterStatsKillHistory : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : "Fhancuz"
		protected System.String Parameter1 {get;set;}
		// From : ["Ganek","Artee","SirCondorX","Daltin","jewzelda","Fhancuz","Skuga","DanBarz","satanboy","Antidepreska","Saola","Taroinha","antivlad","SyrRiall","nobleblood","LordZueira","Joakiga"]
		protected System.String[] Parameter2 {get;set;}
		// From : [1157338701,91260000,346161600,29520000,1029810519,71640000,386712611,38160000,599858153,193751100,50765400,24660000,303041322,270816318,1422834849,98910000,719554908]
		protected System.Int64[] Parameter3 {get;set;}
		// From : ["LSOHtlJGxkGTr6lXl3hbAg==","pM5a8BsV20KlLD5TNLPbRg==","eF4+lSffukO6F+aSAIqgJg==","m9dp3mU3ikaV2TwrSODVoA==","/DMSEiAhd0C50sb3ELXi/g==","4Q7DKVjNPUy3XsyvH9uIwQ==","P2GpPedikE6D7lXXsgwxZw==","V2kzyLok9kylZQg89oOLQA==","sXOUVbp4l0SSraRCLGvi2w==","yvGd1CjDdEyrttIJvo68sQ==","P8sHtjQBqEuH7VPUju1Jgw==","RQqcwT+Wqk27ImfIYbGamA==","huk8tvVrMkmgAQzMvOxKtg==","ZIsAwjBBUE2PKteijCIXyg==","hicjgXpJX0u5EBGjHXzzIg==","+wo7ajFfBUWo1I0BVaKrSQ==","dNi1EkXgdUC/8iBdmIZDrw=="]
		protected System.String[] Parameter4 {get;set;}
		// From : 133
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCharacterStatsKillHistory(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CharacterStatsKillHistory;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToStringArray();
			Parameter3 = data[3].ToIntArray();
			Parameter4 = data[4].ToStringArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
