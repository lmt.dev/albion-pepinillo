using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 105
	public class GEventNewTreasureChest : BaseEvent
	{
		// From : 6163
		protected System.Int64 Parameter0 {get;set;}
		// From : [-225.0,-235.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : "9b28a3b5-64ef-4cf3-81dd-46ff902947dd@1211"
		protected System.String Parameter2 {get;set;}
		// From : 0
		protected System.Int64 Parameter3 {get;set;}
		// From : 105
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewTreasureChest(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewTreasureChest;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
