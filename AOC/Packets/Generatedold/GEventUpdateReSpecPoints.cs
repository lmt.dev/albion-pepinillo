using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 74
	public class GEventUpdateReSpecPoints : BaseEvent
	{
		// From : [0,7637905960,0,0,0]
		protected System.Int64[] Parameter0 {get;set;}
		// From : 74
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateReSpecPoints(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateReSpecPoints;
			Parameter0 = data[0].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
