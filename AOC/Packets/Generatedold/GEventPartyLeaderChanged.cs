using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 216
	public class GEventPartyLeaderChanged : BaseEvent
	{
		// From : 21968
		protected System.Int64 Parameter0 {get;set;}
		// From : "58WDEdGvZEiBgKWEfHf31A=="
		protected System.String Parameter1 {get;set;}
		// From : 216
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyLeaderChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyLeaderChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
