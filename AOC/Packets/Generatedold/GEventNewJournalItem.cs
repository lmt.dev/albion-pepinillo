using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 28
	public class GEventNewJournalItem : BaseEvent
	{
		// From : 1550102
		protected System.Int64 Parameter0 {get;set;}
		// From : 4101
		protected System.Int64 Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter2 {get;set;}
		// From : "DarkolokinhoMeo"
		protected System.String Parameter3 {get;set;}
		// From : 10000
		protected System.Int64 Parameter4 {get;set;}
		// From : 13950000
		protected System.Int64 Parameter6 {get;set;}
		// From : 28
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewJournalItem(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewJournalItem;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
