using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 4
	public class GEventTeleport : BaseEvent
	{
		// From : 1679858
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186248650809958
		protected System.Int64 Parameter1 {get;set;}
		// From : [7.5607657,-7.993052]
		protected System.Single[] Parameter2 {get;set;}
		// From : 137.81096
		protected System.Double Parameter3 {get;set;}
		// From : 5.5
		protected System.Double Parameter4 {get;set;}
		// From : [7.5607657,-7.993052]
		protected System.Single[] Parameter5 {get;set;}
		// From : 4
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTeleport(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.Teleport;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToFloatArray();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToFloatArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
