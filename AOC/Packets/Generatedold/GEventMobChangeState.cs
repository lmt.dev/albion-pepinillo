using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 36
	public class GEventMobChangeState : BaseEvent
	{
		// From : 942872
		protected System.Int64 Parameter0 {get;set;}
		// From : 0
		protected System.Int64 Parameter1 {get;set;}
		// From : 36
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMobChangeState(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.MobChangeState;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
