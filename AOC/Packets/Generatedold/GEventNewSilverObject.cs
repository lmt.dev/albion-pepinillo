using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 33
	public class GEventNewSilverObject : BaseEvent
	{
		// From : 954796
		protected System.Int64 Parameter0 {get;set;}
		// From : 947896
		protected System.Int64 Parameter1 {get;set;}
		// From : true
		protected System.Boolean Parameter2 {get;set;}
		// From : 637186255879744281
		protected System.Int64 Parameter3 {get;set;}
		// From : "SBmRJzE1yk6QDRV0KxR9Hw=="
		protected System.String Parameter4 {get;set;}
		// From : [319.0733,224.86153]
		protected System.Single[] Parameter5 {get;set;}
		// From : 30
		protected System.Int64 Parameter6 {get;set;}
		// From : true
		protected System.Boolean Parameter7 {get;set;}
		// From : 0
		protected System.Int64 Parameter12 {get;set;}
		// From : 0
		protected System.Int64 Parameter13 {get;set;}
		// From : 33
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSilverObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewSilverObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToBoolean();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToString();
			Parameter5 = data[5].ToFloatArray();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToBoolean();
			Parameter12 = data[12].ToInt();
			Parameter13 = data[13].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
