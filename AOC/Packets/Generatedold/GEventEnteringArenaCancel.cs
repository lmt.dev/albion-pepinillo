using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 246
	public class GEventEnteringArenaCancel : BaseEvent
	{
		// From : 1165287
		protected System.Int64 Parameter0 {get;set;}
		// From : true
		protected System.Boolean Parameter1 {get;set;}
		// From : 246
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnteringArenaCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.EnteringArenaCancel;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
