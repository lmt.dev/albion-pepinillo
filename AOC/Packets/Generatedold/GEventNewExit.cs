using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 197
	public class GEventNewExit : BaseEvent
	{
		// From : 2
		protected System.Int64 Parameter0 {get;set;}
		// From : "tKxLQS76cESjw6j8jr6CVA=="
		protected System.String Parameter1 {get;set;}
		// From : [-20.0,-10.0]
		protected System.Single[] Parameter2 {get;set;}
		// From : 197
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewExit(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewExit;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToFloatArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
