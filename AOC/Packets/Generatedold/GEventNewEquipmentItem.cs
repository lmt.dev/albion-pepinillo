using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 25
	public class GEventNewEquipmentItem : BaseEvent
	{
		// From : 327
		protected System.Int64 Parameter0 {get;set;}
		// From : 1883
		protected System.Int64 Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter2 {get;set;}
		// From : ""
		protected System.String Parameter3 {get;set;}
		// From : 2
		protected System.Int64 Parameter4 {get;set;}
		// From : 2980000
		protected System.Int64 Parameter5 {get;set;}
		// From : ""
		protected System.String Parameter6 {get;set;}
		// From : ""
		protected System.String Parameter7 {get;set;}
		// From : 25
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewEquipmentItem(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewEquipmentItem;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
