using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 16
	public class GEventFriendOnlineStatus : BaseEvent
	{
		// From : true
		protected System.Boolean Parameter0 {get;set;}
		// From : "vPV/PIyCNk+pXYp3YmpjDw=="
		protected System.String Parameter1 {get;set;}
		// From : "Krooocker"
		protected System.String Parameter2 {get;set;}
		// From : 272
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFriendOnlineStatus(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FriendOnlineStatus;
			Parameter0 = data[0].ToBoolean();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
