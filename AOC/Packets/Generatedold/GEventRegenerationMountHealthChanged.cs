using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 82
	public class GEventRegenerationMountHealthChanged : BaseEvent
	{
		// From : 2510937
		protected System.Int64 Parameter0 {get;set;}
		// From : 56935161
		protected System.Int64 Parameter1 {get;set;}
		// From : 797.0
		protected System.Double Parameter2 {get;set;}
		// From : 797.0
		protected System.Double Parameter3 {get;set;}
		// From : 7.97
		protected System.Double Parameter4 {get;set;}
		// From : 637186254841031577
		protected System.Int64 Parameter5 {get;set;}
		// From : 82
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationMountHealthChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RegenerationMountHealthChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
