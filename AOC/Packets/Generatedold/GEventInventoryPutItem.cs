using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 22
	public class GEventInventoryPutItem : BaseEvent
	{
		// From : 342
		protected System.Int64 Parameter0 {get;set;}
		// From : 4
		protected System.Int64 Parameter1 {get;set;}
		// From : "5gLc6K9WLk+4JYrAQmgLug=="
		protected System.String Parameter2 {get;set;}
		// From : 22
		protected System.Int64 Parameter252 {get;set;}
		protected GEventInventoryPutItem(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.InventoryPutItem;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
