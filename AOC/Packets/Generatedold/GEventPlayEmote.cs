using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 67
	public class GEventPlayEmote : BaseEvent
	{
		// From : 954992
		protected System.Int64 Parameter0 {get;set;}
		// From : 12
		protected System.Int64 Parameter1 {get;set;}
		// From : 57108504
		protected System.Int64 Parameter3 {get;set;}
		// From : 67
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPlayEmote(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PlayEmote;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
