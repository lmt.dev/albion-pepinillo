using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 45
	public class GEventMinimapZergs : BaseEvent
	{
		// From : [350.7475,4.4990754]
		protected System.Single[] Parameter0 {get;set;}
		// From : "Dg=="
		protected System.String Parameter1 {get;set;}
		// From : "/w=="
		protected System.String Parameter2 {get;set;}
		// From : 301
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMinimapZergs(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.MinimapZergs;
			Parameter0 = data[0].ToFloatArray();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
