using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 70
	public class GEventFishingMiniGame : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 6
		protected System.Int64 Parameter1 {get;set;}
		// From : 637186257792158003
		protected System.Int64 Parameter2 {get;set;}
		// From : 1.0
		protected System.Double Parameter3 {get;set;}
		// From : 0.5
		protected System.Double Parameter4 {get;set;}
		// From : 3.5
		protected System.Double Parameter5 {get;set;}
		// From : 0.8
		protected System.Double Parameter6 {get;set;}
		// From : 1.1
		protected System.Double Parameter7 {get;set;}
		// From : 2.2
		protected System.Double Parameter8 {get;set;}
		// From : 1.0
		protected System.Double Parameter10 {get;set;}
		// From : 3.0
		protected System.Double Parameter11 {get;set;}
		// From : 0.75
		protected System.Double Parameter12 {get;set;}
		// From : 8.0
		protected System.Double Parameter13 {get;set;}
		// From : 4.0
		protected System.Double Parameter15 {get;set;}
		// From : 326
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFishingMiniGame(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FishingMiniGame;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToDouble();
			Parameter6 = data[6].ToDouble();
			Parameter7 = data[7].ToDouble();
			Parameter8 = data[8].ToDouble();
			Parameter10 = data[10].ToDouble();
			Parameter11 = data[11].ToDouble();
			Parameter12 = data[12].ToDouble();
			Parameter13 = data[13].ToDouble();
			Parameter15 = data[15].ToDouble();
			Parameter252 = data[252].ToInt();
		}
	}
}
