using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 50
	public class GEventHarvestFinished : BaseEvent
	{
		// From : 953315
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186256553165051
		protected System.Int64 Parameter1 {get;set;}
		// From : 637186256592972715
		protected System.Int64 Parameter2 {get;set;}
		// From : 7416
		protected System.Int64 Parameter3 {get;set;}
		// From : 2
		protected System.Int64 Parameter4 {get;set;}
		// From : 1
		protected System.Int64 Parameter6 {get;set;}
		// From : 1
		protected System.Int64 Parameter7 {get;set;}
		// From : []
		protected System.Single[] Parameter8 {get;set;}
		// From : ""
		protected System.String Parameter9 {get;set;}
		// From : 50
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestFinished(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.HarvestFinished;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToFloatArray();
			Parameter9 = data[9].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
