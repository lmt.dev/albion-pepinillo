using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 28
	public class GEventNewMountObject : BaseEvent
	{
		// From : 339
		protected System.Int64 Parameter0 {get;set;}
		// From : 1895
		protected System.Int64 Parameter1 {get;set;}
		// From : -1
		protected System.Int64 Parameter2 {get;set;}
		// From : [197.7389,51.834328]
		protected System.Single[] Parameter3 {get;set;}
		// From : 104.69832
		protected System.Double Parameter4 {get;set;}
		// From : "CcxeEDirHk+YRdRgo8iwPg=="
		protected System.String Parameter5 {get;set;}
		// From : 0
		protected System.Int64 Parameter6 {get;set;}
		// From : 3
		protected System.Int64 Parameter7 {get;set;}
		// From : "n/hYzrG/QEKxooIWbbEiBA=="
		protected System.String Parameter9 {get;set;}
		// From : "bMcwYf4DDUil7cjRviSGJg=="
		protected System.String Parameter10 {get;set;}
		// From : 6
		protected System.Int64 Parameter11 {get;set;}
		// From : 11
		protected System.Int64 Parameter12 {get;set;}
		// From : 15
		protected System.Int64 Parameter13 {get;set;}
		// From : 31
		protected System.Int64 Parameter14 {get;set;}
		// From : 16777215
		protected System.Int64 Parameter15 {get;set;}
		// From : 0.55
		protected System.Double Parameter17 {get;set;}
		// From : 284
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewMountObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewMountObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToFloatArray();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter9 = data[9].ToString();
			Parameter10 = data[10].ToString();
			Parameter11 = data[11].ToInt();
			Parameter12 = data[12].ToInt();
			Parameter13 = data[13].ToInt();
			Parameter14 = data[14].ToInt();
			Parameter15 = data[15].ToInt();
			Parameter17 = data[17].ToDouble();
			Parameter252 = data[252].ToInt();
		}
	}
}
