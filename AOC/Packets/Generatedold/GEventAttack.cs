using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 12
	public class GEventAttack : BaseEvent
	{
		// From : 949133
		protected System.Int64 Parameter0 {get;set;}
		// From : 57107561
		protected System.Int64 Parameter1 {get;set;}
		// From : 953314
		protected System.Int64 Parameter2 {get;set;}
		// From : 2
		protected System.Int64 Parameter3 {get;set;}
		// From : 57108409
		protected System.Int64 Parameter4 {get;set;}
		// From : 57108668
		protected System.Int64 Parameter5 {get;set;}
		// From : 12
		protected System.Int64 Parameter252 {get;set;}
		protected GEventAttack(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.Attack;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
