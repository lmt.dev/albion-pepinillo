using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 161
	public class GEventPlayerTradeCancel : BaseEvent
	{
		// From : 4005
		protected System.Int64 Parameter0 {get;set;}
		// From : 161
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPlayerTradeCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PlayerTradeCancel;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
