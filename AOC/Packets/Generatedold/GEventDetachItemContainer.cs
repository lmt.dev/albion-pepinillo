using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 89
	public class GEventDetachItemContainer : BaseEvent
	{
		// From : "cGpGxb6dYUmxTF31/RNdrg=="
		protected System.String Parameter0 {get;set;}
		// From : 89
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDetachItemContainer(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DetachItemContainer;
			Parameter0 = data[0].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
