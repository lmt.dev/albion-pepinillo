using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 104
	public class GEventUpdateChainSpell : BaseEvent
	{
		// From : 954800
		protected System.Int64 Parameter0 {get;set;}
		// From : [351.40704,120.712555]
		protected System.Single[] Parameter1 {get;set;}
		// From : 935416
		protected System.Int64 Parameter2 {get;set;}
		// From : 104
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateChainSpell(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateChainSpell;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
