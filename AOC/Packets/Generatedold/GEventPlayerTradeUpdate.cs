using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 162
	public class GEventPlayerTradeUpdate : BaseEvent
	{
		// From : 4005
		protected System.Int64 Parameter0 {get;set;}
		// From : 9
		protected System.Int64 Parameter1 {get;set;}
		// From : 10000000000
		protected System.Int64 Parameter2 {get;set;}
		// From : ""
		protected System.String Parameter6 {get;set;}
		// From : ""
		protected System.String Parameter7 {get;set;}
		// From : ""
		protected System.String Parameter8 {get;set;}
		// From : []
		protected System.Single[] Parameter9 {get;set;}
		// From : ""
		protected System.String Parameter10 {get;set;}
		// From : ""
		protected System.String Parameter11 {get;set;}
		// From : ""
		protected System.String Parameter12 {get;set;}
		// From : [[-1]]
		protected System.Int64[][] Parameter13 {get;set;}
		// From : [[-1]]
		protected System.Int64[][] Parameter14 {get;set;}
		// From : ""
		protected System.String Parameter15 {get;set;}
		// From : ""
		protected System.String Parameter16 {get;set;}
		// From : ""
		protected System.String Parameter17 {get;set;}
		// From : []
		protected System.Single[] Parameter18 {get;set;}
		// From : ""
		protected System.String Parameter19 {get;set;}
		// From : ""
		protected System.String Parameter20 {get;set;}
		// From : ""
		protected System.String Parameter21 {get;set;}
		// From : [[-1]]
		protected System.Int64[][] Parameter22 {get;set;}
		// From : [[-1]]
		protected System.Int64[][] Parameter23 {get;set;}
		// From : ""
		protected System.String Parameter24 {get;set;}
		// From : 162
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPlayerTradeUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PlayerTradeUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToFloatArray();
			Parameter10 = data[10].ToString();
			Parameter11 = data[11].ToString();
			Parameter12 = data[12].ToString();
			Parameter13 = data[13].ToInt2DArray();
			Parameter14 = data[14].ToInt2DArray();
			Parameter15 = data[15].ToString();
			Parameter16 = data[16].ToString();
			Parameter17 = data[17].ToString();
			Parameter18 = data[18].ToFloatArray();
			Parameter19 = data[19].ToString();
			Parameter20 = data[20].ToString();
			Parameter21 = data[21].ToString();
			Parameter22 = data[22].ToInt2DArray();
			Parameter23 = data[23].ToInt2DArray();
			Parameter24 = data[24].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
