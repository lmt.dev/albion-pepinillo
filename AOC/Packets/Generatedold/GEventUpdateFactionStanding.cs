using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 76
	public class GEventUpdateFactionStanding : BaseEvent
	{
		// From : 2
		protected System.Int64 Parameter0 {get;set;}
		// From : 54000
		protected System.Int64 Parameter1 {get;set;}
		// From : 0
		protected System.Int64 Parameter2 {get;set;}
		// From : 1868593500
		protected System.Int64 Parameter3 {get;set;}
		// From : 76
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateFactionStanding(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateFactionStanding;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
