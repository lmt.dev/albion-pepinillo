using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 86
	public class GEventPersonalSeasonPointsGained : BaseEvent
	{
		// From : 94868
		protected System.Int64 Parameter0 {get;set;}
		// From : 342
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPersonalSeasonPointsGained(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PersonalSeasonPointsGained;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
