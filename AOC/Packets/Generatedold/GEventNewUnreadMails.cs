using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 184
	public class GEventNewUnreadMails : BaseEvent
	{
		// From : true
		protected System.Boolean Parameter0 {get;set;}
		// From : 184
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewUnreadMails(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewUnreadMails;
			Parameter0 = data[0].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
