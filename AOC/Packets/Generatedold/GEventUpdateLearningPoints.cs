using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 73
	public class GEventUpdateLearningPoints : BaseEvent
	{
		// From : 2383826
		protected System.Int64 Parameter0 {get;set;}
		// From : 5890000
		protected System.Int64 Parameter1 {get;set;}
		// From : 637092885816470000
		protected System.Int64 Parameter2 {get;set;}
		// From : 9999990000
		protected System.Int64 Parameter3 {get;set;}
		// From : -20000
		protected System.Int64 Parameter5 {get;set;}
		// From : 73
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateLearningPoints(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateLearningPoints;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
