using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 24
	public class GEventNewCharacter : BaseEvent
	{
		// From : 2519206
		protected System.Int64 Parameter0 {get;set;}
		// From : "Ciebel"
		protected System.String Parameter1 {get;set;}
		// From : 28
		protected System.Int64 Parameter2 {get;set;}
		// From : 1
		protected System.Int64 Parameter4 {get;set;}
		// From : "AQABAQ=="
		protected System.String Parameter5 {get;set;}
		// From : "AAQKBQ=="
		protected System.String Parameter6 {get;set;}
		// From : "qf+5ObfX706tsmi6QVbZZw=="
		protected System.String Parameter7 {get;set;}
		// From : ""
		protected System.String Parameter10 {get;set;}
		// From : ""
		protected System.String Parameter11 {get;set;}
		// From : [-20.0,-20.0]
		protected System.Single[] Parameter12 {get;set;}
		// From : [-20.0,-20.0]
		protected System.Single[] Parameter13 {get;set;}
		// From : 57143478
		protected System.Int64 Parameter14 {get;set;}
		// From : 180.0
		protected System.Double Parameter15 {get;set;}
		// From : 9.075
		protected System.Double Parameter16 {get;set;}
		// From : 2170.0
		protected System.Double Parameter18 {get;set;}
		// From : 2170.0
		protected System.Double Parameter19 {get;set;}
		// From : 144.49364
		protected System.Double Parameter20 {get;set;}
		// From : 57143479
		protected System.Int64 Parameter21 {get;set;}
		// From : 312.0
		protected System.Double Parameter22 {get;set;}
		// From : 312.0
		protected System.Double Parameter23 {get;set;}
		// From : 4.374981
		protected System.Double Parameter24 {get;set;}
		// From : 57143478
		protected System.Int64 Parameter25 {get;set;}
		// From : 669.0
		protected System.Double Parameter26 {get;set;}
		// From : 669.0
		protected System.Double Parameter27 {get;set;}
		// From : 6.69
		protected System.Double Parameter28 {get;set;}
		// From : 57143478
		protected System.Int64 Parameter29 {get;set;}
		// From : 12187.346
		protected System.Double Parameter30 {get;set;}
		// From : 637135093361358065
		protected System.Int64 Parameter31 {get;set;}
		// From : 0
		protected System.Int64 Parameter32 {get;set;}
		// From : [4977,1407,3015,3037,3119,1794,1742,1895,0,0]
		protected System.Int64[] Parameter33 {get;set;}
		// From : [1155,1156,1174,1704,1789,1847,-1,-1,-1,-1,-1,-1,2209,1980]
		protected System.Int64[] Parameter35 {get;set;}
		// From : ""
		protected System.String Parameter43 {get;set;}
		// From : 0
		protected System.Int64 Parameter45 {get;set;}
		// From : [-1,-1,25,55,73,-1,81]
		protected System.Int64[] Parameter48 {get;set;}
		// From : 24
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewCharacter(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewCharacter;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter10 = data[10].ToString();
			Parameter11 = data[11].ToString();
			Parameter12 = data[12].ToFloatArray();
			Parameter13 = data[13].ToFloatArray();
			Parameter14 = data[14].ToInt();
			Parameter15 = data[15].ToDouble();
			Parameter16 = data[16].ToDouble();
			Parameter18 = data[18].ToDouble();
			Parameter19 = data[19].ToDouble();
			Parameter20 = data[20].ToDouble();
			Parameter21 = data[21].ToInt();
			Parameter22 = data[22].ToDouble();
			Parameter23 = data[23].ToDouble();
			Parameter24 = data[24].ToDouble();
			Parameter25 = data[25].ToInt();
			Parameter26 = data[26].ToDouble();
			Parameter27 = data[27].ToDouble();
			Parameter28 = data[28].ToDouble();
			Parameter29 = data[29].ToInt();
			Parameter30 = data[30].ToDouble();
			Parameter31 = data[31].ToInt();
			Parameter32 = data[32].ToInt();
			Parameter33 = data[33].ToIntArray();
			Parameter35 = data[35].ToIntArray();
			Parameter43 = data[43].ToString();
			Parameter45 = data[45].ToInt();
			Parameter48 = data[48].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
