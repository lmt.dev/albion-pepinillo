using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 75
	public class GEventUpdateCurrency : BaseEvent
	{
		// From : 3
		protected System.Int64 Parameter0 {get;set;}
		// From : 1
		protected System.Int64 Parameter1 {get;set;}
		// From : -30000000
		protected System.Int64 Parameter2 {get;set;}
		// From : 0
		protected System.Int64 Parameter3 {get;set;}
		// From : 110160000
		protected System.Int64 Parameter4 {get;set;}
		// From : 75
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateCurrency(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateCurrency;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
