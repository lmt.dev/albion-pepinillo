using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 127
	public class GEventGuildMemberTerritoryUpdate : BaseEvent
	{
		// From : "3a90c837-96de-40e4-95ab-9d19131bfa58@1313"
		protected System.String Parameter0 {get;set;}
		// From : true
		protected System.Boolean Parameter1 {get;set;}
		// From : 127
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGuildMemberTerritoryUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GuildMemberTerritoryUpdate;
			Parameter0 = data[0].ToString();
			Parameter1 = data[1].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
