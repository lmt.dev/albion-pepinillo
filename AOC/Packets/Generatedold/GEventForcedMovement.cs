using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 130
	public class GEventForcedMovement : BaseEvent
	{
		// From : 953689
		protected System.Int64 Parameter0 {get;set;}
		// From : 2
		protected System.Int64 Parameter1 {get;set;}
		// From : 637186255257624989
		protected System.Int64 Parameter2 {get;set;}
		// From : 56977049
		protected System.Int64 Parameter3 {get;set;}
		// From : 11.0
		protected System.Double Parameter4 {get;set;}
		// From : [336.6773,62.07404,338.8891,60.876625]
		protected System.Single[] Parameter5 {get;set;}
		// From : 2859
		protected System.Int64 Parameter6 {get;set;}
		// From : 1
		protected System.Int64 Parameter7 {get;set;}
		// From : 130
		protected System.Int64 Parameter252 {get;set;}
		protected GEventForcedMovement(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ForcedMovement;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToFloatArray();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
