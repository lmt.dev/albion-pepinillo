using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 13
	public class GEventCastStart : BaseEvent
	{
		// From : 2518543
		protected System.Int64 Parameter0 {get;set;}
		// From : 57135961
		protected System.Int64 Parameter1 {get;set;}
		// From : [0.0,0.0]
		protected System.Single[] Parameter2 {get;set;}
		// From : 57135961
		protected System.Int64 Parameter3 {get;set;}
		// From : 711
		protected System.Int64 Parameter4 {get;set;}
		// From : -1
		protected System.Int64 Parameter5 {get;set;}
		// From : 2518543
		protected System.Int64 Parameter6 {get;set;}
		// From : 1
		protected System.Int64 Parameter7 {get;set;}
		// From : 13
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CastStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToFloatArray();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
