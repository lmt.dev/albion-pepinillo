using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 43
	public class GEventPlayerMovementRateUpdate : BaseEvent
	{
		// From : 100
		protected System.Int64 Parameter0 {get;set;}
		// From : 299
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPlayerMovementRateUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PlayerMovementRateUpdate;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
