using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 30
	public class GEventMountCooldownUpdate : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186257248271022
		protected System.Int64 Parameter1 {get;set;}
		// From : 58295971
		protected System.Int64 Parameter2 {get;set;}
		// From : 286
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMountCooldownUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.MountCooldownUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
