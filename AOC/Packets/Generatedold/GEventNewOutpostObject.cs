using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 74
	public class GEventNewOutpostObject : BaseEvent
	{
		// From : 6500
		protected System.Int64 Parameter0 {get;set;}
		// From : "FOREST_RED_FACTION_WARFARE_OUTPOST_TOWER"
		protected System.String Parameter1 {get;set;}
		// From : "OUTPOST_FOREST"
		protected System.String Parameter2 {get;set;}
		// From : [84.0,246.0]
		protected System.Single[] Parameter3 {get;set;}
		// From : 2
		protected System.Int64 Parameter4 {get;set;}
		// From : 2
		protected System.Int64 Parameter5 {get;set;}
		// From : 9223372036854775807
		protected System.Int64 Parameter6 {get;set;}
		// From : 330
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewOutpostObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewOutpostObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToFloatArray();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
