using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 245
	public class GEventEnteringArenaStart : BaseEvent
	{
		// From : 1165287
		protected System.Int64 Parameter0 {get;set;}
		// From : 245
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnteringArenaStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.EnteringArenaStart;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
