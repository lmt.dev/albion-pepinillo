using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 8
	public class GEventDamageShieldUpdate : BaseEvent
	{
		// From : 2481310
		protected System.Int64 Parameter0 {get;set;}
		// From : 8
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDamageShieldUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DamageShieldUpdate;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
