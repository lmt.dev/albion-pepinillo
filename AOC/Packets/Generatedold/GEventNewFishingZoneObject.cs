using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 69
	public class GEventNewFishingZoneObject : BaseEvent
	{
		// From : 253
		protected System.Int64 Parameter0 {get;set;}
		// From : [208.0,31.1]
		protected System.Single[] Parameter1 {get;set;}
		// From : 4
		protected System.Int64 Parameter2 {get;set;}
		// From : 1
		protected System.Int64 Parameter3 {get;set;}
		// From : "FishingNodeOceanSwarm"
		protected System.String Parameter4 {get;set;}
		// From : 325
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewFishingZoneObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewFishingZoneObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
