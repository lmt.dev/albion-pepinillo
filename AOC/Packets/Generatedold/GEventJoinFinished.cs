using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 2
	public class GEventJoinFinished : BaseEvent
	{
		// From : 2
		protected System.Int64 Parameter252 {get;set;}
		protected GEventJoinFinished(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.JoinFinished;
			Parameter252 = data[252].ToInt();
		}
	}
}
