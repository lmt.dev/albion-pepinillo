using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 151
	public class GEventDied : BaseEvent
	{
		// From : [0.0,0.0]
		protected System.Single[] Parameter0 {get;set;}
		// From : 1157274
		protected System.Int64 Parameter1 {get;set;}
		// From : "Tempesty"
		protected System.String Parameter2 {get;set;}
		// From : 1157274
		protected System.Int64 Parameter3 {get;set;}
		// From : "Tempesty"
		protected System.String Parameter4 {get;set;}
		// From : "A ordem dos assassinos"
		protected System.String Parameter5 {get;set;}
		// From : true
		protected System.Boolean Parameter6 {get;set;}
		// From : 151
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDied(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.Died;
			Parameter0 = data[0].ToFloatArray();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToString();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
