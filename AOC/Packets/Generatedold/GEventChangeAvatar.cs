using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 147
	public class GEventChangeAvatar : BaseEvent
	{
		// From : 2373602
		protected System.Int64 Parameter0 {get;set;}
		// From : 27
		protected System.Int64 Parameter1 {get;set;}
		// From : 147
		protected System.Int64 Parameter252 {get;set;}
		protected GEventChangeAvatar(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ChangeAvatar;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
