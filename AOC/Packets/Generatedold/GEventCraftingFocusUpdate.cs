using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 9
	public class GEventCraftingFocusUpdate : BaseEvent
	{
		// From : 2505796
		protected System.Int64 Parameter0 {get;set;}
		// From : 56820947
		protected System.Int64 Parameter1 {get;set;}
		// From : 30000.0
		protected System.Double Parameter3 {get;set;}
		// From : 9
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCraftingFocusUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CraftingFocusUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter3 = data[3].ToDouble();
			Parameter252 = data[252].ToInt();
		}
	}
}
