using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 84
	public class GEventRegenerationHealthEnergyComboChanged : BaseEvent
	{
		// From : 949133
		protected System.Int64 Parameter0 {get;set;}
		// From : 57113188
		protected System.Int64 Parameter1 {get;set;}
		// From : 1355.0
		protected System.Double Parameter2 {get;set;}
		// From : 1355.0
		protected System.Double Parameter3 {get;set;}
		// From : 637186256621301960
		protected System.Int64 Parameter5 {get;set;}
		// From : 267.0
		protected System.Double Parameter6 {get;set;}
		// From : 267.0
		protected System.Double Parameter7 {get;set;}
		// From : 8.0
		protected System.Double Parameter8 {get;set;}
		// From : 637186256621301960
		protected System.Int64 Parameter9 {get;set;}
		// From : 84
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationHealthEnergyComboChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RegenerationHealthEnergyComboChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToDouble();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToDouble();
			Parameter7 = data[7].ToDouble();
			Parameter8 = data[8].ToDouble();
			Parameter9 = data[9].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
