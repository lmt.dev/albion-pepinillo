using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 23
	public class GEventInventoryDeleteItem : BaseEvent
	{
		// From : 2505808
		protected System.Int64 Parameter0 {get;set;}
		// From : 2
		protected System.Int64 Parameter1 {get;set;}
		// From : 23
		protected System.Int64 Parameter252 {get;set;}
		protected GEventInventoryDeleteItem(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.InventoryDeleteItem;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
