using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 71
	public class GEventUpdateMoney : BaseEvent
	{
		// From : 2518387
		protected System.Int64 Parameter0 {get;set;}
		// From : 23636273334
		protected System.Int64 Parameter1 {get;set;}
		// From : 71
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateMoney(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateMoney;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
