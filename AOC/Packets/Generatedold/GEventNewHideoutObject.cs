using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 120
	public class GEventNewHideoutObject : BaseEvent
	{
		// From : 4978
		protected System.Int64 Parameter0 {get;set;}
		// From : "iieNlK5l2EGtpzEpM2ZoMw=="
		protected System.String Parameter1 {get;set;}
		// From : "HIDEOUT_STEPPE_DEAD"
		protected System.String Parameter3 {get;set;}
		// From : [-300.0,120.0]
		protected System.Single[] Parameter4 {get;set;}
		// From : "bv1FiTkzxU+l25XQU+eEnA=="
		protected System.String Parameter6 {get;set;}
		// From : "Killer Instinct"
		protected System.String Parameter7 {get;set;}
		// From : "qExXyPnpkEiPNKoSR0oUVQ=="
		protected System.String Parameter8 {get;set;}
		// From : "RANG"
		protected System.String Parameter9 {get;set;}
		// From : "Leyos"
		protected System.String Parameter10 {get;set;}
		// From : 637162173444558873
		protected System.Int64 Parameter11 {get;set;}
		// From : 1
		protected System.Int64 Parameter12 {get;set;}
		// From : 4
		protected System.Int64 Parameter13 {get;set;}
		// From : 2
		protected System.Int64 Parameter14 {get;set;}
		// From : 0
		protected System.Int64 Parameter16 {get;set;}
		// From : 637185504000000000
		protected System.Int64 Parameter17 {get;set;}
		// From : 637166525248308359
		protected System.Int64 Parameter19 {get;set;}
		// From : 9223372036854775807
		protected System.Int64 Parameter20 {get;set;}
		// From : 637185687308970016
		protected System.Int64 Parameter21 {get;set;}
		// From : 376
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewHideoutObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewHideoutObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToFloatArray();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToString();
			Parameter10 = data[10].ToString();
			Parameter11 = data[11].ToInt();
			Parameter12 = data[12].ToInt();
			Parameter13 = data[13].ToInt();
			Parameter14 = data[14].ToInt();
			Parameter16 = data[16].ToInt();
			Parameter17 = data[17].ToInt();
			Parameter19 = data[19].ToInt();
			Parameter20 = data[20].ToInt();
			Parameter21 = data[21].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
