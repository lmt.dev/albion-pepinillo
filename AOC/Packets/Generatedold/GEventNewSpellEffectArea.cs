using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 102
	public class GEventNewSpellEffectArea : BaseEvent
	{
		// From : 9413
		protected System.Int64 Parameter0 {get;set;}
		// From : [387.0,0.0]
		protected System.Single[] Parameter2 {get;set;}
		// From : 717
		protected System.Int64 Parameter4 {get;set;}
		// From : 1
		protected System.Int64 Parameter5 {get;set;}
		// From : 179895
		protected System.Int64 Parameter6 {get;set;}
		// From : 179895
		protected System.Int64 Parameter7 {get;set;}
		// From : 0
		protected System.Int64 Parameter8 {get;set;}
		// From : 102
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSpellEffectArea(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewSpellEffectArea;
			Parameter0 = data[0].ToInt();
			Parameter2 = data[2].ToFloatArray();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
