using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 248
	public class GEventEnteringArenaLockCancel : BaseEvent
	{
		// From : 1164101
		protected System.Int64 Parameter0 {get;set;}
		// From : 248
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnteringArenaLockCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.EnteringArenaLockCancel;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
