using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 68
	public class GEventNewFloatObject : BaseEvent
	{
		// From : 346
		protected System.Int64 Parameter0 {get;set;}
		// From : [210.0373,29.078363]
		protected System.Single[] Parameter1 {get;set;}
		// From : 147.27275
		protected System.Double Parameter2 {get;set;}
		// From : 325
		protected System.Int64 Parameter3 {get;set;}
		// From : 5
		protected System.Int64 Parameter4 {get;set;}
		// From : 324
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewFloatObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewFloatObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
