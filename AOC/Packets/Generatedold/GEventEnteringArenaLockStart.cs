using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 247
	public class GEventEnteringArenaLockStart : BaseEvent
	{
		// From : 1165287
		protected System.Int64 Parameter0 {get;set;}
		// From : 247
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnteringArenaLockStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.EnteringArenaLockStart;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
