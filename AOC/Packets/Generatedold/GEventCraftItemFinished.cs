using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 61
	public class GEventCraftItemFinished : BaseEvent
	{
		// From : 2505796
		protected System.Int64 Parameter0 {get;set;}
		// From : 1752
		protected System.Int64 Parameter1 {get;set;}
		// From : [2506309]
		protected System.Int64[] Parameter2 {get;set;}
		// From : ""
		protected System.String Parameter3 {get;set;}
		// From : ""
		protected System.String Parameter4 {get;set;}
		// From : 61
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCraftItemFinished(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CraftItemFinished;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToIntArray();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
