using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 27
	public class GEventReputationImplicationUpdate : BaseEvent
	{
		// From : 951595
		protected System.Int64 Parameter0 {get;set;}
		// From : 56991898
		protected System.Int64 Parameter1 {get;set;}
		// From : 283
		protected System.Int64 Parameter252 {get;set;}
		protected GEventReputationImplicationUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ReputationImplicationUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
