using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 52
	public class GEventTerritoryClaimStart : BaseEvent
	{
		// From : 63521
		protected System.Int64 Parameter0 {get;set;}
		// From : 52939174
		protected System.Int64 Parameter1 {get;set;}
		// From : 52954174
		protected System.Int64 Parameter2 {get;set;}
		// From : 4593
		protected System.Int64 Parameter3 {get;set;}
		// From : 308
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTerritoryClaimStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.TerritoryClaimStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
