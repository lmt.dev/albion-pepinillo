using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 78
	public class GEventOverChargeEnd : BaseEvent
	{
		// From : 2510825
		protected System.Int64 Parameter0 {get;set;}
		// From : 1
		protected System.Int64 Parameter1 {get;set;}
		// From : 2
		protected System.Int64 Parameter2 {get;set;}
		// From : 334
		protected System.Int64 Parameter252 {get;set;}
		protected GEventOverChargeEnd(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.OverChargeEnd;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
