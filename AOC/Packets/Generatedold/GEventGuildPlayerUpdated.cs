using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 93
	public class GEventGuildPlayerUpdated : BaseEvent
	{
		// From : "G4zC2YpQcES6ktoOz+1xQA=="
		protected System.String Parameter0 {get;set;}
		// From : "Woozyyy"
		protected System.String Parameter1 {get;set;}
		// From : true
		protected System.Boolean Parameter2 {get;set;}
		// From : 637186258760333662
		protected System.Int64 Parameter3 {get;set;}
		// From : 1
		protected System.Int64 Parameter4 {get;set;}
		// From : 26
		protected System.Int64 Parameter5 {get;set;}
		// From : 16
		protected System.Int64 Parameter6 {get;set;}
		// From : 0
		protected System.Int64 Parameter8 {get;set;}
		// From : 93
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGuildPlayerUpdated(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GuildPlayerUpdated;
			Parameter0 = data[0].ToString();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToBoolean();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter8 = data[8].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
