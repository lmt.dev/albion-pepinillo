using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 77
	public class GEventRespawn : BaseEvent
	{
		// From : 2470172
		protected System.Int64 Parameter0 {get;set;}
		// From : 56809066
		protected System.Int64 Parameter1 {get;set;}
		// From : [-54.733135,29.342123]
		protected System.Single[] Parameter2 {get;set;}
		// From : 203.18211
		protected System.Double Parameter3 {get;set;}
		// From : 2471.0
		protected System.Double Parameter4 {get;set;}
		// From : 2472.0
		protected System.Double Parameter5 {get;set;}
		// From : 40.695225
		protected System.Double Parameter6 {get;set;}
		// From : 56809066
		protected System.Int64 Parameter7 {get;set;}
		// From : 216.0
		protected System.Double Parameter8 {get;set;}
		// From : 296.0
		protected System.Double Parameter9 {get;set;}
		// From : 4.2200937
		protected System.Double Parameter10 {get;set;}
		// From : 56808897
		protected System.Int64 Parameter11 {get;set;}
		// From : true
		protected System.Boolean Parameter12 {get;set;}
		// From : true
		protected System.Boolean Parameter13 {get;set;}
		// From : 77
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRespawn(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.Respawn;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToFloatArray();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToDouble();
			Parameter6 = data[6].ToDouble();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToDouble();
			Parameter9 = data[9].ToDouble();
			Parameter10 = data[10].ToDouble();
			Parameter11 = data[11].ToInt();
			Parameter12 = data[12].ToBoolean();
			Parameter13 = data[13].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
