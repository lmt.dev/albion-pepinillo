using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 18
	public class GEventCastHit : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 325
		protected System.Int64 Parameter1 {get;set;}
		// From : 1971
		protected System.Int64 Parameter2 {get;set;}
		// From : 1
		protected System.Int64 Parameter3 {get;set;}
		// From : 0
		protected System.Int64 Parameter4 {get;set;}
		// From : 18
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastHit(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CastHit;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
