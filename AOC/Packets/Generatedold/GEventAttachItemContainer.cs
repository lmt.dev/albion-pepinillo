using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 88
	public class GEventAttachItemContainer : BaseEvent
	{
		// From : 269
		protected System.Int64 Parameter0 {get;set;}
		// From : "cGpGxb6dYUmxTF31/RNdrg=="
		protected System.String Parameter1 {get;set;}
		// From : "Beq2qVTgxEurBYaZdu75/Q=="
		protected System.String Parameter2 {get;set;}
		// From : "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="
		protected System.String Parameter3 {get;set;}
		// From : 64
		protected System.Int64 Parameter4 {get;set;}
		// From : 88
		protected System.Int64 Parameter252 {get;set;}
		protected GEventAttachItemContainer(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.AttachItemContainer;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
