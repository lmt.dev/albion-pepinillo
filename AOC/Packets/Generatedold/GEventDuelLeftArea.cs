using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 172
	public class GEventDuelLeftArea : BaseEvent
	{
		// From : 63429
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186209685277907
		protected System.Int64 Parameter1 {get;set;}
		// From : 172
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDuelLeftArea(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DuelLeftArea;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
