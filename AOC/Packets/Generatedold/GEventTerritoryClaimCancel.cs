using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 53
	public class GEventTerritoryClaimCancel : BaseEvent
	{
		// From : 63521
		protected System.Int64 Parameter0 {get;set;}
		// From : 309
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTerritoryClaimCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.TerritoryClaimCancel;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
