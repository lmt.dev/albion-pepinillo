using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 111
	public class GEventCustomizationChanged : BaseEvent
	{
		// From : 2479010
		protected System.Int64 Parameter0 {get;set;}
		// From : "AgAAAAY="
		protected System.String Parameter1 {get;set;}
		// From : "AAABAAU="
		protected System.String Parameter2 {get;set;}
		// From : 367
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCustomizationChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CustomizationChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
