using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 27
	public class GEventNewFurnitureItem : BaseEvent
	{
		// From : 1550129
		protected System.Int64 Parameter0 {get;set;}
		// From : 1980
		protected System.Int64 Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter2 {get;set;}
		// From : "SYSTEM"
		protected System.String Parameter3 {get;set;}
		// From : 2700000
		protected System.Int64 Parameter4 {get;set;}
		// From : 27
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewFurnitureItem(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewFurnitureItem;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
