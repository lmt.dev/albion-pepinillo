using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 48
	public class GEventHarvestStart : BaseEvent
	{
		// From : 953315
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186256593005051
		protected System.Int64 Parameter1 {get;set;}
		// From : 637186256593005051
		protected System.Int64 Parameter2 {get;set;}
		// From : 7416
		protected System.Int64 Parameter3 {get;set;}
		// From : 8
		protected System.Int64 Parameter4 {get;set;}
		// From : 3.98
		protected System.Double Parameter5 {get;set;}
		// From : 953349
		protected System.Int64 Parameter6 {get;set;}
		// From : 1822
		protected System.Int64 Parameter7 {get;set;}
		// From : 48
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.HarvestStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToDouble();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
