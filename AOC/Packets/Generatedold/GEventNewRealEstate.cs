using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 174
	public class GEventNewRealEstate : BaseEvent
	{
		// From : 1353
		protected System.Int64 Parameter0 {get;set;}
		// From : "rDG7IWh+5E6Jt2SKfBAhMw=="
		protected System.String Parameter1 {get;set;}
		// From : [-65.0,-45.0]
		protected System.Single[] Parameter2 {get;set;}
		// From : [20.0,20.0]
		protected System.Single[] Parameter3 {get;set;}
		// From : "E9M88knFzEi/WV1/RACrGw=="
		protected System.String Parameter4 {get;set;}
		// From : "Zzy"
		protected System.String Parameter5 {get;set;}
		// From : 0
		protected System.Int64 Parameter7 {get;set;}
		// From : "June"
		protected System.String Parameter9 {get;set;}
		// From : -10000
		protected System.Int64 Parameter10 {get;set;}
		// From : 571521400000
		protected System.Int64 Parameter11 {get;set;}
		// From : 636359039297583732
		protected System.Int64 Parameter12 {get;set;}
		// From : 174
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewRealEstate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewRealEstate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToFloatArray();
			Parameter3 = data[3].ToFloatArray();
			Parameter4 = data[4].ToString();
			Parameter5 = data[5].ToString();
			Parameter7 = data[7].ToInt();
			Parameter9 = data[9].ToString();
			Parameter10 = data[10].ToInt();
			Parameter11 = data[11].ToInt();
			Parameter12 = data[12].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
