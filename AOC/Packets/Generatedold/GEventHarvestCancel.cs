using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 49
	public class GEventHarvestCancel : BaseEvent
	{
		// From : 124900
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186175439695051
		protected System.Int64 Parameter1 {get;set;}
		// From : 49
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.HarvestCancel;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
