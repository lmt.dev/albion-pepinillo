using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 115
	public class GEventRecoveryVaultPlayerInfo : BaseEvent
	{
		// From : 5
		protected System.Int64 Parameter0 {get;set;}
		// From : "0e860295-bb5c-4be1-89d0-9b1bbcba0b99@1001"
		protected System.String Parameter1 {get;set;}
		// From : []
		protected System.Single[] Parameter2 {get;set;}
		// From : []
		protected System.Single[] Parameter3 {get;set;}
		// From : []
		protected System.Single[] Parameter4 {get;set;}
		// From : ""
		protected System.String Parameter5 {get;set;}
		// From : []
		protected System.Single[] Parameter6 {get;set;}
		// From : ""
		protected System.String Parameter7 {get;set;}
		// From : ""
		protected System.String Parameter8 {get;set;}
		// From : 371
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRecoveryVaultPlayerInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RecoveryVaultPlayerInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToFloatArray();
			Parameter3 = data[3].ToFloatArray();
			Parameter4 = data[4].ToFloatArray();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToFloatArray();
			Parameter7 = data[7].ToString();
			Parameter8 = data[8].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
