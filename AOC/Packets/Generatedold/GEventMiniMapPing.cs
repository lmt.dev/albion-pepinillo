using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 165
	public class GEventMiniMapPing : BaseEvent
	{
		// From : [115.701004,-122.29211]
		protected System.Single[] Parameter0 {get;set;}
		// From : 0
		protected System.Int64 Parameter1 {get;set;}
		// From : 165
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMiniMapPing(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.MiniMapPing;
			Parameter0 = data[0].ToFloatArray();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
