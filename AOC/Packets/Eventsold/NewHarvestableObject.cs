﻿using System;
using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class NewHarvestableObject : GEventNewHarvestableObject
    {
        protected NewHarvestableObject(Dictionary<byte, object> data) : base(data)
        {

            //        Key = 10, Value = 2 //count. If not set its empty
            //        Key = 0, Value = 7589
            //        Key = 2, Value = 636712223127023853
            //        Key = 5, Value = 11
            //        Key = 6, Value = -1
            //        Key = 7, Value = 6
            //        Key = 8, Value = System.Single[]
            //        Key = 9, Value = 270
            //        Key = 11, Value = 1
            //        Key = 252, Value = 30
            Id = Parameter0;
            TypeHarvestable = Parameter5;
            Tier = Parameter7;
            Loc = Parameter8;
            Carges = Parameter11;



        }
        public long Id { get; }
        public long TypeHarvestable { get; }
        public long Tier { get; }
        public float[] Loc { get; }
        public long Carges { get; }
    }
}