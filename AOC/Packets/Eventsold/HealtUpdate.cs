﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class HealtUpdate : GEventHealthUpdate
    {
        protected HealtUpdate(Dictionary<byte, object> data) : base(data)
        {
            MobId = Parameter0;
            NewHp = Parameter5;


        }

        public long MobId { get; }
        public float NewHp { get; set; }

       

    }
}