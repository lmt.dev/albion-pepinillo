﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class OtherGrabbedLoot : GEventOtherGrabbedLoot
    {
        protected OtherGrabbedLoot(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            VictimName = Parameter1; //may be null
            LooterName = Parameter2;
            ItemIndex = Parameter4;
            Cantidad = Parameter5;
        }

        public long Id { get; }
        public string VictimName { get; }
        public string LooterName { get; }
        public long ItemIndex { get; }
        public long Cantidad { get; }
    }
}
