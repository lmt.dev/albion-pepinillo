﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class MobChangeState : GEventMobChangeState
    {
        protected MobChangeState(Dictionary<byte, object> data) : base(data)
        {
            MobId = Parameter0;
            EnchantmentLevel = Parameter1;


        }

        public long MobId { get; }
        public float EnchantmentLevel { get; set; }

       

    }
}