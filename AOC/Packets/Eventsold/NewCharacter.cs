﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class NewCharacter : GEventNewCharacter
    {
        protected NewCharacter(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            Flag = Parameter45;
            Name = Parameter1;
            Guild = Parameter8; //may be null
            Alliance = Parameter43;
            ItemIndexList = Parameter33;
            Position1 = Parameter12;
            Position2 = Parameter13;

        }

        public long Id { get; }
        public long Flag { get; }
        public string Name { get; }
        public object Guild { get; }
        public string Alliance { get; }
        public long[] ItemIndexList { get; }
        public float[] Position1 { get; }
        public float[] Position2 { get; }
    }
}
