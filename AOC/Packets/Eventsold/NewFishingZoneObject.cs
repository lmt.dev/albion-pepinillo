﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class NewFishingZoneObject : GEventNewFishingZoneObject
    {
        protected NewFishingZoneObject(Dictionary<byte, object> data) : base(data)
        {
            X = Parameter1[0];
            Y = Parameter1[1];
            Amount = Parameter2;
            Type = Parameter4;

        }

        public float X { get; }
        public float Y { get; private set; }
        public long Amount { get; set; }
        public string A { get; set; }
        public string Type { get; set; }

    }
}