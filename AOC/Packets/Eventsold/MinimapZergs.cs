﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class MinimapZergs : GEventMinimapZergs
    {
        protected MinimapZergs(Dictionary<byte, object> data) : base(data)
        {
            //when 2 blob appear there are 4 values in Parameter0 {X1,Y1,X2,Y2}
            //same in sizes y colours
            CordinatesPairs = Parameter0;
            Sizes = Parameter1; 
            Colours = Parameter2; 

        }

        public float[] CordinatesPairs { get; }
        public short[] Sizes { get; } 
        public short[] Colours { get; }


       

    }
}