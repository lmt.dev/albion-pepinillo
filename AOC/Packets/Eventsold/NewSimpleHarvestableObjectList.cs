﻿using System;
using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class NewSimpleHarvestableObjectList : GEventNewSimpleHarvestableObjectList
    {
        protected NewSimpleHarvestableObjectList(Dictionary<byte, object> data) : base(data)
        {
            IdList = Parameter0;//may be Byte[] or Int16[];
            TypeList = Parameter1;
            TierList = Parameter2;
            LocList = Parameter3;
            SizeList = Parameter4;


        }
        public long[] IdList { get; }
        public Byte[] TypeList { get; }
        public Byte[] TierList { get; }
        public Single[] LocList { get; }
        public Byte[] SizeList { get; }
    }
}