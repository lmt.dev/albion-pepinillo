﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class NewRandomDungeonExit : GEventNewRandomDungeonExit
    {
        protected NewRandomDungeonExit(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            X = Parameter1[0];
            Y = Parameter1[1];
            Name = Parameter3;


        }

        public long Id { get; }
        public float X { get; set; }
        public float Y { get; set; }
        public string Name { get; }
    }
}