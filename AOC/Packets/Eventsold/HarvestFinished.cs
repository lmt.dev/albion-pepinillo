﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class HarvestFinished : GEventHarvestFinished
    {
        protected HarvestFinished(Dictionary<byte, object> data) : base(data)
        {
            IdHarvestable = Parameter1;

        }

        public long IdHarvestable { get; }
    }
}