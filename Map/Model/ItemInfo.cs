﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Map.Model
{
    class ItemInfo
    {
        public static Dictionary<string, ItemObject> itemDictionary = new Dictionary<string, ItemObject>();

        public ItemObject GetItem(string key)
        {
            if (itemDictionary.ContainsKey(key))
            {
                return itemDictionary[key];
            }
            return null;
        }
    }


}
