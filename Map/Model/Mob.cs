﻿

using System.Windows.Media;

namespace Map.Model
{
    class Mob : Entity
    {

        public Color[] fontPerColor = {
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.Black,
                Colors.Black,
                Colors.Black

            };

        public Brush[] chargesBrushes = {
             Brushes.Transparent,
             Brushes.Green,
             Brushes.Blue,
             Brushes.Purple
            };

        public Brush[] harvestBrushes = new Brush[9]
            {
            Brushes.Black,
            Brushes.Gray,
            Brushes.Gray,
            Brushes.Gray,
            Brushes.Blue,
            Brushes.Red,
            Brushes.Coral,
            Brushes.Goldenrod,
            Brushes.Silver
            };

        public long Health { get; internal set; }
		public MobInfo MobInfo { get; internal set; }
		public float EnchantmentLevel {get; internal set; }



       



    }
}
