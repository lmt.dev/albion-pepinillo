﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace Map.Model
{
    internal class Entity
    {

        public bool Drawed { get; internal set; }
        public bool Remove { get; internal set; }
        public long Id { get; internal set; }
        public float X { get; internal set; }
        public float Y { get; internal set; }
        public float YMap { get; internal set; }
        public float XMap { get; internal set; }
        public float XClickMap { get; internal set; }
        public float YClickMap { get; internal set; }
        public bool Moved { get; internal set; }

        public float[] Loc { get; internal set; }
        public float[] Loc2 { get; internal set; }
        public float Pos { get; internal set; }
        public float Pos1 { get; internal set; }
        public Ellipse Icon { get; private set; }
        public Image ImageIcon { get; private set; }

        public Grid Grid { get; set; }

        public MapInfo mapInfo { get; set; }

        public Entity()
        {
            this.Drawed = false;
            this.Moved = false;
            this.Remove = false;

        }

        public void setMapCoords(float FixedWidth, float FixedHeight, float scale,float localPlayerX, float localPlayerY)
        {

            float mobX = FixedWidth / 2f + scale * (-1f * X + localPlayerX);
            float mobY = FixedHeight / 2f + scale * (Y - localPlayerY);
            Moved = false;
            if(XMap != null || YMap != null)
            {
                if(XMap==mobX && YMap == mobY)
                {
                    
                }
                else
                {
                    Moved = true;
                    XMap = mobX;
                    YMap = mobY;
                }
                if (Loc2 != null) 
                {
                    XClickMap = FixedWidth / 2f + scale * (-1f * Loc2[0] + localPlayerX);
                    YClickMap = FixedHeight / 2f + scale * (Loc2[1] - localPlayerY); 
                }


            }

        }


        public float[] GetLoc()
        {
            return Loc;
        }

        internal void SetPos(float value)
        {
            Pos = value;
          
        }
        internal void SetPos1(float value)
        {
            Pos1 = value;

        }

        internal void SetLoc(float[] value)
        {
            Loc = value;
            X = value[0];
            Y = value[1];
        }
        internal void SetLoc2(float[] value)
        {
            Loc2 = value;
        }

        internal void addIcon(Ellipse ellipse)
        {
            this.Icon = ellipse;
        }

        internal void addImageIcon(Image image)
        {
            this.ImageIcon = image;

        }


    }
}