﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Map.Util
{
    public class __pSettings
    {
        public bool OpcT1 { get; set; }
        public bool OpcT2 { get; set; }
        public bool OpcT3 { get; set; }
        public bool OpcT4 { get; set; }
        public bool OpcT5 { get; set; }
        public bool OpcT6 { get; set; }
        public bool OpcT7 { get; set; }
        public bool OpcT8 { get; set; }
        public bool ShowChars { get; set; }
        public bool ShowEquip { get; set; }
        public bool SoundChars { get; set; }
        public bool ShowRol { get; set; }
        public bool ShowFlag { get; set; }
        public bool ShowFiber { get; set; }
        public bool ShowWood { get; set; }
        public bool ShowRock { get; set; }
        public bool ShowOre { get; set; }
        public bool ShowFish { get; set; }
        public bool ShowOthers { get; set; }
        public bool ShowSkinables { get; set; }
        public bool ShowHarvestables { get; set; }
        public bool ShowTreasures { get; set; }
        public double Width { get; set; }

        public long Lang { get; set; }

        public double Height { get; set; }
        public bool ShowIcons { get; set; }
        public string username { get; set; }
        public string SniffingDevice { get; set; }

    }
}
