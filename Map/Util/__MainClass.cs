﻿using System;
using System.Collections.Generic;
using AOC.Network.Packets.Implemented.Event;
using Map.Handlers;
using Map.Model;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Map.Util;
using System.Media;
using System.IO;

namespace Map
{
    class __MainClass
    {
        private static bool run;
        private static EntityHandler eHandler;

		public static bool Run { get => run; set => run = value; }
		private  __IMySettings Settings { get; set; }

		internal static void BTest(EntityHandler _eHandler, SharpPcap.ICaptureDevice deviceSelected, string token)
        {





			eHandler = _eHandler;

			__AOCSniffer aOCSniffer = new __AOCSniffer(token);
			aOCSniffer.Join += onJoin;
			aOCSniffer.JoinFinished += OnJoinFinished;
			aOCSniffer.Move += OnPlayerMove;
            aOCSniffer.MoveEvent += OnEntityMove;
            aOCSniffer.MobChangeState += OnMobChangeState;
            aOCSniffer.NewRandomDungeonExit += onNewRandomDungeonExit;
            aOCSniffer.NewFishingZoneObject += onNewFishingZoneObject;
            aOCSniffer.HarvestFinished += OnHarvestFinished;
            aOCSniffer.NewCharacter += onNewCharacter;
            aOCSniffer.NewMob += onNewMob;
            aOCSniffer.UpdateFame += onUpdateFame;
            aOCSniffer.NewHarvestableObject += onNewHarvestableObject;
            aOCSniffer.NewSimpleHarvestableObjectList += onNewSimpleHarvestableObjectList;
            aOCSniffer.Died += onDied;
            aOCSniffer.Leave += onLeave;
            aOCSniffer.HealthUpdate += onHealthUpdate;
            aOCSniffer.__Subscribe();


			Run = true;
			while (Run)
			{
				try
				{
					aOCSniffer.__StartListening(deviceSelected);
					Thread.Sleep(500);
				}
				catch (Exception ex)
				{
					//Console.WriteLine(ex);
					//Thread.Sleep(4000);
				}
			}
		}

		


		private static void onNewFishingZoneObject(object request, object ev)
        {
			var fs = (__NewFishingZoneObject)ev;
			eHandler.addFishSpot(fs);

		}
        private static void onNewRandomDungeonExit(object request, object ev)
		{
			try
			{
				__NewRandomDungeonExit newRandomDungeonExit = (__NewRandomDungeonExit)ev;
                //int id = int.Parse(parameters[0].ToString());
                //float[] a = (float[])parameters[1];
                //float posX = a[0];
                //float posY = a[1];
                
                //bool bandera = (bool)parameters[4];
                //DungeonHandler.AddDungeon(id, posX, posY, texto, bandera);
                eHandler.addDungeon(newRandomDungeonExit);
				
			}
			catch (Exception ex)
			{
				//Console.WriteLine(ex);
			}
		}

		private static void OnMobChangeState(object request, object ev)
		{
			var mobChangeState = (__MobChangeState)ev;

			try
			{
				int mobId = 0;
				byte enchantmentLevel = 0;
				//if (!int.TryParse(joinFinished.MobId, out mobId)) return;
				//if (!byte.TryParse(parameters[1].ToString(), out enchantmentLevel)) return;
				//eHandler.UpdateMobEnchantmentLevel(mobId, enchantmentLevel);
				eHandler.UpdateMobEnchantmentLevel(mobChangeState.MobId, mobChangeState.EnchantmentLevel);
			}
			catch { }
		}

		private static void OnJoinFinished(object request, object ev)
		{
			try
			{
				eHandler.EntityLeaveAll();
				eHandler.LocalPlayer.Zfame = 0;

			}
			catch (Exception ex)
			{ }
			eHandler.LocalPlayer.Teleporting = false;
		}

		private static void onUpdateFame(object request, object ev)
		{
			var updateFame = (__UpdateFame)ev;

			try
			{
				var compVal = int.Parse(updateFame.FameAmount.ToString()) * 0.00015;
				eHandler.LocalPlayer.Fame += (long)compVal;
				eHandler.LocalPlayer.Zfame += (long)compVal;
				eHandler.LocalPlayer.DrawFame();
			}
			catch (Exception ex)
			{

			}
		}





        private static void OnPlayerMove(object request, object ev)
        {
			var move = (__Move)ev;
            try
            {
                new Thread(() =>
                {
                    if (eHandler != null)
                    {
						eHandler.LocalPlayer.SetLoc(move.Loc);
						eHandler.LocalPlayer.SetLoc2(move.NewLoc);
						eHandler.LocalPlayer.Moved = true;
					}
                   
                }).Start();

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }





        private static void onJoin(object request, object ev)
		{
			__Join join = (__Join)ev;
			eHandler.LocalPlayer.Name = join.Jugador;
			try
			{
				//val(join);
				

				new Thread(() =>
				{

					eHandler.LocalPlayer.Teleporting = true;
					MapInfo mapInfo = null;
					var mapa_actual = "";
					var map_info = MapInfo.getMapInfo(join.IdMap);
					if (map_info != null)
					{
						mapa_actual = map_info.UniqueName;
						mapa_actual = join.IdMap.Trim();
					}
					else
					{
						mapa_actual = join.IdMap.Trim();
					}

					if (mapInfo != null)
					{
						//PlayerHandler.UpdateLocalPlayerMap(mapa_actual);
						eHandler.LocalPlayer.mapInfo = mapInfo;
						eHandler.LocalPlayer.SetLoc((float[])join.FullParams[9]);
						//solicitarDatos(join.Jugador, mapa_actual, "1");
					}


				}).Start();

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);

			}
		}



		private static void OnHarvestFinished(object request, object ev)
		{
			var harvestFinished = (__HarvestFinished)ev;
			try
			{
				//int harvestableId = int.Parse(harvestFinished.Parameters[3].ToString());

				//float count = 0;
				//try { count = float.Parse(parameters[7].ToString()); } catch (Exception ex) { }

				if (harvestFinished.Count < 1)
				{
					//HarvestableHandler.RemoveHarvestable(harvestableId);
					eHandler.EntityLeave(harvestFinished.IdHarvestable);

				}
			}
			catch (Exception ex)
			{

			}
		}

		private static void OnEntityMove(object request, object ev)
		{
			var moveEvent = (__MoveEvent)ev;

			try
			{
				eHandler.moveEntity(moveEvent);

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);

			}

		}

		private static void onNewCharacter(object request, object ev)
		{
			var newCharacter = (__NewCharacter)ev;
			try
			{
				Player player = new Player();
				player.Id = newCharacter.Id;
				player.Loc = newCharacter.Position2;
				player.Loc2 = newCharacter.Position1;
				player.Name = newCharacter.Name;
				player.Alliance = newCharacter.Alliance;
				player.Guild = newCharacter.Guild;
				player.Flag = newCharacter.Flag;
				player.isPK = newCharacter.isPK;
				player.ItemIndexList = newCharacter.ItemIndexList;
				eHandler.addPlayer(player);
	}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);

			}

		}

		private static void onNewHarvestableObject(object request, object ev)
		{
			var harvestable = (__NewHarvestableObject)ev;

			try
			{
				Harvestable h = new Harvestable();
				h.Id = harvestable.Id;
				h.Loc = harvestable.Loc;
				h.X = harvestable.Loc[0];
				h.Y = harvestable.Loc[1];
				h.Type = harvestable.Type;
				h.Tier = harvestable.Tier;
				h.Size = harvestable.Size;
				h.Charges = harvestable.Charges;
				eHandler.addHarvestable(h);

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);

			}

		}

		private static void onDied(object request, object ev)
		{
			var died = (__Died)ev;

			try
			{
				eHandler.EntityLeave(died.IdVictim);

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);

			}
		}

		private static void onLeave(object request, object ev)
		{
			var leave = (__Leave)ev;

			try
			{
				eHandler.EntityLeave(leave.Id);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
			}
		}

		private static void onHealthUpdate(object request, object ev)
		{
			var update = (__HealthUpdate)ev;
			try
			{
				eHandler.HealthUpdate(update.MobId, update.NewHp);

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);

			}
		}

		private static void onNewSimpleHarvestableObjectList(object request, object ev)
		{
			try
			{
				__NewSimpleHarvestableObjectList harvestableList = (__NewSimpleHarvestableObjectList)ev;

				List<int> a0 = new List<int>();
				if (harvestableList.Parameters[0].GetType() == typeof(byte[]))
				{
					byte[] array = (byte[])harvestableList.Parameters[0];
					foreach (byte b2 in array)
					{
						a0.Add(b2);
					}
				}
				else if (harvestableList.Parameters[0].GetType() == typeof(short[]))
				{
					short[] array2 = (short[])harvestableList.Parameters[0];
					foreach (short b in array2)
					{
						a0.Add(b);
					}
				}

				try
				{
					byte[] types = (byte[])harvestableList.Parameters[1];
					byte[] tiers = (byte[])harvestableList.Parameters[2];
					float[] locs = (float[])harvestableList.Parameters[3];
					byte[] sizes = (byte[])harvestableList.Parameters[4];
					for (int i = 0; i < a0.Count; i++)
					{
						int id = a0.ElementAt(i);
						byte type = types[i];
						byte tier = tiers[i];
						float posX = locs[i * 2];
						float posY = locs[i * 2 + 1];
						byte count = sizes[i];
						byte charges = 0;
						Harvestable h = new Harvestable();
						h.Id = id;
						h.Type = type;
						h.Tier = tier;
						h.Loc = new float[2] { posX, posY };
						h.X = posX;
						h.Y = posY;
						h.Size = count;
						h.Charges = charges;
						eHandler.addHarvestable(h);
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex);

				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);


			}

		}

		private static void onNewMob(object request, object ev)
		{
			try
			{
                __NewMob mob = (__NewMob)ev;
                eHandler.addMob(mob);

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);

			}
		}


	}
}
