﻿namespace Map.Util
{
    public interface __IMySettings
    {
        bool OpcT1 { get; set; }
        bool OpcT2 { get; set; }
        bool OpcT3 { get; set; }
        bool OpcT4 { get; set; }
        bool OpcT5 { get; set; }
        bool OpcT6 { get; set; }
        bool OpcT7 { get; set; }
        bool OpcT8 { get; set; }
        bool ShowChars { get; set; }
        bool ShowEquip { get; set; }
        bool SoundChars { get; set; }
        bool ShowRol { get; set; }
        bool ShowFlag { get; set; }
        bool ShowFiber { get; set; }
        bool ShowWood { get; set; }
        bool ShowRock { get; set; }
        bool ShowOre { get; set; }
        bool ShowFish { get; set; }
        bool ShowOthers { get; set; }
        bool ShowSkinables { get; set; }
        bool ShowHarvestables { get; set; }
        bool ShowTreasures { get; set; }
        double Width { get; set; }

        long Lang { get; set; }

        double Height { get; set; }
        bool ShowIcons { get; set; }
        string username { get; set; }
        string SniffingDevice { get; set; }
    }
}
