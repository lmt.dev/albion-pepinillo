﻿using Config.Net;
using Map.Util;
using RadialMenu.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Map
{
    /// <summary>
    /// Lógica de interacción para MenuWindow.xaml
    /// </summary>
    public partial class __MenuWindow : Window, INotifyPropertyChanged
    {
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private bool _isOpen1 = false;

        private Lang text;


        private __IMySettings Settings;




        public bool IsOpen1
        {
            get
            {
                return _isOpen1;
            }
            set
            {
                _isOpen1 = value;
                RaisePropertyChanged();
            }
        }
        public ICommand CloseRadialMenu1
        {
            get
            {
                return new RelayCommand(() => IsOpen1 = false);
            }
        }
        public ICommand OpenRadialMenu1
        {
            get
            {
                return new RelayCommand(() => { IsOpen1 = true; });
            }
        }


        public ICommand espSet
        {
            get
            {
                return new RelayCommand(() => { text.SetLang(0); dibujarMenu(); });
            }
        }
        public ICommand ptpSet
        {
            get
            {
                return new RelayCommand(() => { text.SetLang(1); dibujarMenu(); });
            }
        }
        public ICommand enpSet
        {
            get
            {
                return new RelayCommand(() => { text.SetLang(2); dibujarMenu(); });
            }
        }

        public ICommand checkT1
        { 
            get
            { 
                        return new RelayCommand(() => {
                            Settings.OpcT1 = !Settings.OpcT1;
                        });
            }
        }

        public ICommand Test3
        {
            get
            {
                return new RelayCommand(() => System.Diagnostics.Debug.WriteLine("3"));
            }
        }

        public ICommand Test4
        {
            get
            {
                return new RelayCommand(() => System.Diagnostics.Debug.WriteLine("4"));
            }
        }

        public ICommand Test5
        {
            get
            {
                return new RelayCommand(() => System.Diagnostics.Debug.WriteLine("5"));
            }
        }

        public ICommand Test6
        {
            get
            {
                return new RelayCommand(
                    () =>
                    {
                        System.Diagnostics.Debug.WriteLine("6");
                    }
                    //,() => { return false;  }// To disable the 6th item
                );
            }
        }

        public void dibujarMenu()
        {

            IsOpen1 = false;
            Task.Delay(400);

            // Main menu
            var MainMenuItems = new List<RadialMenuItem> {
                    new RadialMenuItem
                    {
                        Content = new TextBlock { Text = text.getString("tier_menu") }, //0
                    },
                    new RadialMenuItem
                    {
                        Content = new TextBlock { Text = text.getString("players_menu") }, //1
                    },
                    new RadialMenuItem
                    {
                        Content = new TextBlock { Text = text.getString("ress_menu") }, //2

                    },
                    new RadialMenuItem
                    {
                        Content = new TextBlock { Text = text.getString("mobs_menu") }, //3

                    },
                    new RadialMenuItem
                    {
                        Content = new TextBlock { Text = text.getString("lang_menu") }, //4

                    },
                    new RadialMenuItem
                    {
                        Content = new TextBlock { Text = text.getString("graph_menu") }, //5

                    },

                };

            // Sub menu
            var langSubMenu = new List<RadialMenuItem>
            {
                new RadialMenuItem
                {
                    Content = new TextBlock { Text = "ESPAÑOL" },
                    Command = espSet
                },
                new RadialMenuItem
                {
                    Content = new TextBlock { Text = "PORTUGUES" },

                    Command = ptpSet
                },
                new RadialMenuItem
                {
                    Content = new TextBlock { Text = "ENGLISH" },
                    Command = enpSet
                },
            };

            setMenuButtonChecked(langSubMenu[0], text.GetLang()==0);
            setMenuButtonChecked(langSubMenu[1], text.GetLang()==1);
            setMenuButtonChecked(langSubMenu[2], text.GetLang()==2);


            var tierSubMenu = new List<RadialMenuItem>();
            for (int i = 0; i <= 7; i++)
            {
                tierSubMenu.Add(new RadialMenuItem
                {
                    Content = new TextBlock { Text = "T"+((int)i+1) },

                });
            }


            setMenuButtonChecked(tierSubMenu[0], Settings.OpcT1);
            setMenuButtonChecked(tierSubMenu[1], Settings.OpcT2);
            setMenuButtonChecked(tierSubMenu[2], Settings.OpcT3);
            setMenuButtonChecked(tierSubMenu[3], Settings.OpcT4);
            setMenuButtonChecked(tierSubMenu[4], Settings.OpcT5);
            setMenuButtonChecked(tierSubMenu[5], Settings.OpcT6);
            setMenuButtonChecked(tierSubMenu[6], Settings.OpcT7);
            setMenuButtonChecked(tierSubMenu[7], Settings.OpcT8);


            //            for(int i = 0; i < 9; i++)
            for (int i = 0; i <= 7; i++)
            {
                tierSubMenu[i].Click += async (sender, args) =>
                {
                    bool current = false;
                    var s = sender.ToString().Replace("RadialMenu.Controls.RadialMenuItem: ","");

                    switch (s.ToString())
                    {
                        case "T1":

                            Settings.OpcT1 = !Settings.OpcT1;
                            current = Settings.OpcT1;
                            break;
                        case "T2":
                            Settings.OpcT2 = !Settings.OpcT2;
                            current = Settings.OpcT2;
                            break;
                        case "T3":
                            Settings.OpcT3 = !Settings.OpcT3;
                            current = Settings.OpcT3;
                            break;
                        case "T4":
                            Settings.OpcT4 = !Settings.OpcT4;
                            current = Settings.OpcT4;
                            break;
                        case "T5":
                            Settings.OpcT5 = !Settings.OpcT5;
                            current = Settings.OpcT5;
                            break;
                        case "T6":
                            Settings.OpcT6 = !Settings.OpcT6;
                            current = Settings.OpcT6;
                            break;
                        case "T7":
                            Settings.OpcT7 = !Settings.OpcT7;
                            current = Settings.OpcT7;
                            break;
                        case "T8":
                            Settings.OpcT8 = !Settings.OpcT8;
                            current = Settings.OpcT8;
                            break;

                    }
                    IsOpen1 = false;
                    setMenuButtonChecked((RadialMenuItem)sender, current);
                    await Task.Delay(0);
                    IsOpen1 = true;
                };
            }





            var closeCentralItem = new RadialMenuCentralItem
            {

                Content = new Rectangle
                {
                    Height = 20,
                    Width = 20,
                    Fill = new VisualBrush()
                    {
                        Visual = (Visual)Resources["appbar_close"],
                    }
                }

            };
            var backCentralItem = new RadialMenuCentralItem
            {
                Content = new Rectangle
                {
                    Height = 20,
                    Width = 20,
                    Fill = new VisualBrush()
                    {
                        Visual = (Visual)Resources["appbar_home_location_round"],
                    }
                }
            };


            var charSubMenu = new List<RadialMenuItem>
            {
                new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("players_menu_show") },
                },
                 new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("players_menu_sound") },

                },
                  new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("players_menu_equip") },

                },
                   new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("players_menu_rol") },

                },

            };
            setMenuButtonChecked(charSubMenu[0], Settings.ShowChars);
            setMenuButtonChecked(charSubMenu[1], Settings.SoundChars);
            setMenuButtonChecked(charSubMenu[2], Settings.ShowEquip);
            setMenuButtonChecked(charSubMenu[3], Settings.ShowRol);


            var ressSubMenu = new List<RadialMenuItem>
            {
                new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("ress_menu_fiber") },

                },
                 new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("ress_menu_wood") },

                },
                  new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("ress_menu_rock") },

                },
                   new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("ress_menu_ore") },

                },
                    new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("ress_menu_fish") },

                },
                     new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("ress_menu_treasures") },

                },
            };

            setMenuButtonChecked(ressSubMenu[0], Settings.ShowFiber);
            setMenuButtonChecked(ressSubMenu[1], Settings.ShowWood);
            setMenuButtonChecked(ressSubMenu[2], Settings.ShowRock);
            setMenuButtonChecked(ressSubMenu[3], Settings.ShowOre);
            setMenuButtonChecked(ressSubMenu[4], Settings.ShowFish);
            setMenuButtonChecked(ressSubMenu[5], Settings.ShowTreasures);

            var npcSubMenu = new List<RadialMenuItem>
            {
                new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("mobs_menu_skin") },

                },
                 new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("mobs_menu_harvest") },

                },
                  new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("mobs_menu_other") },

                },
            };

            var graphSubMenu = new List<RadialMenuItem>
            {
                 new RadialMenuItem()
                {
                    Content = new TextBlock { Text = text.getString("graph_menu_icons") },

                },
            };

            setMenuButtonChecked(npcSubMenu[0], Settings.ShowSkinables);
            setMenuButtonChecked(npcSubMenu[1], Settings.ShowHarvestables);
            setMenuButtonChecked(npcSubMenu[2], Settings.ShowOthers);


            //Char SubMenu Clicks
            charSubMenu[0].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowChars = !Settings.ShowChars;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowChars);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            charSubMenu[1].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.SoundChars = !Settings.SoundChars;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.SoundChars);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            charSubMenu[2].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowEquip = !Settings.ShowEquip;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowEquip);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            charSubMenu[3].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowRol = !Settings.ShowRol;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowRol);
                await Task.Delay(0);
                IsOpen1 = true;
            };

            ressSubMenu[0].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowFiber = !Settings.ShowFiber;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowFiber);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            ressSubMenu[1].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowWood = !Settings.ShowWood;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowWood);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            ressSubMenu[2].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowRock = !Settings.ShowRock;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowRock);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            ressSubMenu[3].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowOre = !Settings.ShowOre;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowOre);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            ressSubMenu[4].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowFish = !Settings.ShowFish;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowFish);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            ressSubMenu[5].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowTreasures = !Settings.ShowTreasures;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowTreasures);
                await Task.Delay(0);
                IsOpen1 = true;
            };

            npcSubMenu[0].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowSkinables = !Settings.ShowSkinables;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowSkinables);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            npcSubMenu[1].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowHarvestables = !Settings.ShowHarvestables;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowHarvestables);
                await Task.Delay(0);
                IsOpen1 = true;
            };
            npcSubMenu[2].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowOthers = !Settings.ShowOthers;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowOthers);
                await Task.Delay(0);
                IsOpen1 = true;
            };

            //grapsh submenu clicks

            graphSubMenu[0].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                Settings.ShowIcons = !Settings.ShowIcons;
                setMenuButtonChecked((RadialMenuItem)sender, Settings.ShowIcons);
                await Task.Delay(0);
                IsOpen1 = true;
            };







            //go to tier submenu
            MainMenuItems[0].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                MyRadialMenu.Items = tierSubMenu;
                MyRadialMenu.CentralItem = backCentralItem;
                IsOpen1 = true;
            };

            // Go to character submenu
            MainMenuItems[1].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                MyRadialMenu.Items = charSubMenu;
                MyRadialMenu.CentralItem = backCentralItem;
                IsOpen1 = true;
            };
            // Go to resources submenu
            MainMenuItems[2].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                MyRadialMenu.Items = ressSubMenu;
                MyRadialMenu.CentralItem = backCentralItem;
                IsOpen1 = true;
            };
            // Go to npc submenu
            MainMenuItems[3].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                MyRadialMenu.Items = npcSubMenu;
                MyRadialMenu.CentralItem = backCentralItem;
                IsOpen1 = true;
            };

            // Go to lang submenu
            MainMenuItems[4].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                MyRadialMenu.Items = langSubMenu;
                MyRadialMenu.CentralItem = backCentralItem;
                IsOpen1 = true;
            };

            // Go to graph submenu
            MainMenuItems[5].Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                MyRadialMenu.Items = graphSubMenu;
                MyRadialMenu.CentralItem = backCentralItem;
                IsOpen1 = true;
            };



            // center back
            backCentralItem.Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                MyRadialMenu.Items = MainMenuItems;
                MyRadialMenu.CentralItem = closeCentralItem;
                IsOpen1 = true;
            };
            closeCentralItem.Click += async (sender, args) =>
            {
                IsOpen1 = false;
                await Task.Delay(400);
                Hide();
                IsOpen1 = true;
            };






            // Set default menu to Main menu
            MyRadialMenu.Items = MainMenuItems;
            MyRadialMenu.CentralItem = closeCentralItem;
            IsOpen1 = true;


        }

        private void setMenuButtonChecked(RadialMenuItem radialMenuItem, bool value)
        {
            if (value)
                radialMenuItem.Style = (Style)MyRadialMenu.Resources["checkedItem"];
            else
                radialMenuItem.Style = (Style)MyRadialMenu.Resources["uncheckedItem"];
        }

        public __MenuWindow(Lang lang)
        {

            var path = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");

            Settings = new ConfigurationBuilder<__IMySettings>()
                .UseJsonFile(path + "\\appsettings.json")
                .Build();

            DataContext = this;

            InitializeComponent();
            text = lang;
            dibujarMenu();

            //KeyDown += new KeyEventHandler(MainWindow_KeyDown);
            //MouseDown += Window_MouseDown;


        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = (Window)sender;
            window.Topmost = true;
            OpenRadialMenu1.Execute(null);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {


        }
    }
}
